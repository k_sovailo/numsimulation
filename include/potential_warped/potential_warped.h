/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */ 

#pragma once

#include <numsimulation/common.h>
#include <vector>
#include <string>
#include <Eigen/Dense>

namespace potential_warped
{
    /// Smallest element of the simulation. It is a point with potential defined on it
    struct Point
    {
        double potential;       ///< Potential
        double new_potential;   ///< New potential, used in Jacobi algorithm
        double a_dp_dxi2;       ///< Precomputed coefficient for dP/dXi second derivative
        double a_dp_deta2;      ///< Precomputed coefficient for dP/dEta second derivative
        double a_dp_dxieta;     ///< Precomputed coefficient for dP/dXi/dEta second derivative
        double a_dp_dxi;        ///< Precomputed coefficient for dP/dXi derivative
        double dp_deta;         ///< Precomputed coefficient for dP/dEta derivative
    };
    
    /// Parameters of the simulation
    struct AbstractParameters
    {
        // Geometry parameters
        double min[2];                                                  ///< Lower boundaries of the simulation field (xi/eta)
        double max[2];                                                  ///< Upper boundaries of the simulation field (xi/eta)
        virtual Eigen::Vector2d get_xy(Eigen::Vector2d xieta) const = 0;///< Get X/Y coordinates from Xi/Eta (must be implemented)
        virtual double get_dxi_dx(Eigen::Vector2d xieta) const = 0;     ///< Get dXi/dX derivative from Xi and Eta (must be implemented)
        virtual double get_dxi_dy(Eigen::Vector2d xieta) const = 0;     ///< Get dXi/dY derivative from Xi and Eta (must be implemented)
        virtual double get_deta_dx(Eigen::Vector2d xieta) const = 0;    ///< Get dEta/dX derivative from Xi and Eta (must be implemented)
        virtual double get_deta_dy(Eigen::Vector2d xieta) const = 0;    ///< Get dEta/dY derivative from Xi and Eta (must be implemented)
        virtual double get_dxi_dx2(Eigen::Vector2d xieta) const = 0;    ///< Get dXi/dX second order derivative from Xi and Eta (must be implemented)
        virtual double get_dxi_dy2(Eigen::Vector2d xieta) const = 0;    ///< Get dXi/dY second order derivative from Xi and Eta (must be implemented)
        virtual double get_deta_dx2(Eigen::Vector2d xieta) const = 0;   ///< Get dEta/dX second order derivative from Xi and Eta (must be implemented)
        virtual double get_deta_dy2(Eigen::Vector2d xieta) const = 0;   ///< Get dEta/dY second order derivative from Xi and Eta (must be implemented)
        
        // Grid parameters
        unsigned int n[2];          ///< Number of points
        
        // Solver parameters
        unsigned int max_iteration; ///< Maximum iteration
        double max_residual;        ///< Maximum residual
        double beta;                ///< Undershooting/overshooting parameter
        bool gauss;                 ///< Defines whether Gauss-Seidel orJacobi algorithm is used
        bool exact;                 ///< Potential function is valid on entire field, used for testing

        AbstractParameters(numsimulation::ParameterReader &reader); ///< Reads abstract parameters from parameter reader
        virtual double potential(Eigen::Vector2d xy) const = 0;     ///< Potential function (must be implemented)
    };

    /// Solves potential flow problem on field, which is a rectangular in xi/eta and warped by predefined function
    class Solver
    {
    private:
        const AbstractParameters *_parameters;      ///< Simulation parameters
        const std::string _filename;                ///< Filename to save results
        std::vector<std::vector<Point>> _points;    ///< Simulation field
        
        Eigen::Vector2d _get_xieta(unsigned int xii, unsigned int etai) const;  ///< Get point coordinates (in xi/eta) based on it's number

    public:
        Solver(const AbstractParameters *parameters, std::string filename); ///< Creates and initializes solver
        void solve();                                                       ///< Solves the problem
        void output();                                                      ///< Writes output to file
    };
}