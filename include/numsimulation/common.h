/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */ 

#pragma once

#include <string>
#include <map>
#include <vector>
#include <Eigen/Dense>

namespace numsimulation
{
    /// Reader of parameter files
    class ParameterReader
    {
    private:
        struct Parameter
        {
            std::string value;
            bool used;
        };
        std::map<std::string, Parameter> _parameters;   ///< Read parameters

    public:    
        ParameterReader(std::string filename);          ///< Creates parameter reader
        std::string get_string(std::string name);       ///< Gets text parameter
        double get_real(std::string name);              ///< Gets real parameter
        unsigned int get_integer(std::string name);     ///< Gets integer parameters
        void warn_unused();                             ///< Print warnings about unused variables
    };

    /// Square of a number
    inline double sqr(double a) noexcept { return a * a; }
    /// Square of a number
    inline unsigned int sqr(unsigned int a) noexcept { return a * a; }

    /// Grid type
    enum class GridType
    {
        triangular,
        square,
        hexagonal
    };

    /// Element position for either triangle, square or hexagon element
    struct Position { int xi = 0, yi = 0; bool upside_down = false; bool operator<(const Position &b) const; };
    unsigned int get_shape(GridType typ);                                                                                   ///< Gets number of points/faces
    Eigen::Vector2d get_center(Position position, GridType typ, Eigen::Vector2d origin, const double size[2]);              ///< Gets center of the element
    std::vector<Eigen::Vector2d> get_points(Position position, GridType typ, Eigen::Vector2d origin, const double size[2]); ///< Gets points of the element
    struct FaceNeighbor { Position position; unsigned int face; };
    FaceNeighbor get_face_neighbor(Position position, GridType typ, unsigned int face);                                     ///< Gets neighbors of the face
    struct PointNeighbor { Position position; unsigned int point; };
    std::vector<PointNeighbor> get_point_neighbors(Position position, GridType typ, unsigned int point);                    ///< Gets neighbors of the point

    /// Intersection between figure and segment
    struct Intersection
    {
        bool valid;             ///< Whether teh intersection is valid
        Eigen::Vector2d coord;  ///< Coordinate of the intersection
        Eigen::Vector2d vector; ///< Vector of the surface
        Eigen::Vector2d normal; ///< Normal of the surface

        Intersection();                                                                     ///< Creates invalid intersection
        Intersection(Eigen::Vector2d coord, Eigen::Vector2d vector, Eigen::Vector2d normal);///< Creates intersection
    };

    /// Abstract figure
    class Figure
    {
    public:
        virtual Intersection intersection(Eigen::Vector2d a, Eigen::Vector2d b) = 0;    ///< Searches for intersection between figure and line
    };

    /// Circle figure
    class Circle : public Figure
    {
    private:
        Eigen::Vector2d _center;    ///< Coordinates of the center of the circle
        double _radius;             ///< Radius of the circle
        bool _normal_inside;        ///< Normal is pointing inside of the circle
    public:
        virtual Intersection intersection(Eigen::Vector2d a, Eigen::Vector2d b);///< Searches for intersection between figure and line
        Circle(Eigen::Vector2d center, double radius, bool normal_inside);      ///< Creates circle
    };

    /// Arc figure
    class Arc : public Figure
    {
    private:
        Eigen::Vector2d _center;    ///< Coordinates of the center of the arc
        double _radius;             ///< Radius of the arc
        bool _normal_inside;        ///< Normal is pointing inside of the arc
        double _azimuth;            ///< Azimuth of the beginning of the arc
        double _angle;              ///< Angle of the arc, counterclockwise
    public:
        virtual Intersection intersection(Eigen::Vector2d a, Eigen::Vector2d b);                        ///< Searches for intersection between figure and line
        Arc(Eigen::Vector2d center, double radius, double azimuth, double angle, bool normal_inside);   ///< Creates arc
    };

    /// Line figure
    class Line : public Figure
    {
    private:
        Eigen::Vector2d _a; ///< Beginning of the line
        Eigen::Vector2d _b; ///< Ending of the line
        bool _normal_cw;    ///< Normal is pointing clockwise of the line vector
    public:
        virtual Intersection intersection(Eigen::Vector2d a, Eigen::Vector2d b);    ///< Searches for intersection between figure and line
        Line(Eigen::Vector2d a, Eigen::Vector2d b, bool normal_cw);                 ///< Creates line
    };

    Eigen::Vector2d rotate_cw(Eigen::Vector2d v);   ///< Rotates vector clockwise
    Eigen::Vector2d rotate_ccw(Eigen::Vector2d v);  ///< Rotates vector counterclockwise
}

/** @mainpage Numsimulation
Numsimulation is project developed as part of Numerical Flow Simulation course at RWTH Aachen university. It is a collection of seven software libraries with every one responsible for its own type of problem.

@section Dependencies
 - The library depends on [Eigen](https://eigen.tuxfamily.org) for its core functionality.
 - [CMake](https://cmake.org) is used for compilation and further automation
 - [Python](https://www.python.org), [NumPy](https://numpy.org) and [Matplotlib](https://matplotlib.org) are used for visualization. 
 - [Doxygen](https://www.doxygen.nl) is used for generation of documentation
 - [LaTeX](https://www.latex-project.org) and [Latexmk](https://mg.readthedocs.io/latexmk.html) are used for report generation

@section Build
@code{.sh}
mkdir build
cd build
cmake ..
cmake --build .
@endcode

@section Usage
@code{.sh}
cd build
cmake --build . --target demo   # Run all tests and visualize results
cmake --build . --target doc    # Generate documentation
cmake --build . --target report # Generate report
@endcode
*/