/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */ 

#pragma once

#include <numsimulation/common.h>
#include <set>
using namespace numsimulation;

namespace potential
{
    class TemporaryPoint;

    ///Boundary type
    enum class BoundaryType
    {
        fixed,  ///< Fixed potential
        wall,   ///< No flow
        free    ///< No boundary
    };

    ///Boundary geometry and boundary conditions
    struct Boundary
    {
        Figure *figure;     ///< Boundary geometry
        BoundaryType typ;   ///< Boundary type
    };

    ///Smallest element of the simulation. It is a point with potential defined on it.
    struct Point
    {
        Eigen::Vector2d coord;                          ///< Coordinate
        BoundaryType typ;                               ///< Type
        double potential;                               ///< Potential
        double new_potential;                           ///< New potential, used in Jacobi algorithm
        std::vector<Point*> neighbors;                  ///< Neighboring points
        Eigen::VectorXd b;                              ///< Precalculated coefficients for second potential derivative
        Point(Eigen::Vector2d coord, BoundaryType typ); ///< Creates point
    };

    ///Parameters (parameter functions unimplemented)
    struct AbstractParameters
    {
        //Geometry parameters
        std::vector<Boundary> boundaries;           ///< List of boundaries

        //Grid parameters
        Eigen::Vector2d grid_origin;                ///< Grid origin, where the field probing begins
        GridType grid_type;                         ///< Grid type
        double grid_size[2];                        ///< Space between two neighboring points

        //Solver parameters
        unsigned int max_iteration; ///< Maximum iteration
        double max_residual;        ///< Maximum residual
        double beta;                ///< Undershooting/overshooting parameter
        bool exact;                 ///< Potential function is valid on entire field, used for testing
        bool gauss;                 ///< Defines whether Gauss-Seidel orJacobi algorithm is used

        AbstractParameters(numsimulation::ParameterReader &reader); ///< Reads abstract parameters from parameter reader
        virtual double potential(Eigen::Vector2d coord) const = 0;  ///< Potential function (must be implemented)
    };

    ///Generalized solver of potential flow
    class Solver
    {
    private:
        const AbstractParameters *_parameters;
        const std::string _filename;
        std::set<Point*> _points;

        void _add_first_point(std::map<Position, TemporaryPoint> &active);                                              ///< Adds first point to active set
        void _add_all_points(std::map<Position, TemporaryPoint> &active, std::map<Position, TemporaryPoint> &passive);  ///< Searches and adds points to passive set
        void _create_points(std::map<Position, TemporaryPoint> &points);                                                ///< Creates points and add to set
        void _interconnect_points(std::map<Position, TemporaryPoint> &points);                                          ///< Search and connect neighbors
        void _precalculate_coefficients(std::map<Position, TemporaryPoint> &points);                                    ///< Precalculated coefficients
        void _set_potential(std::map<Position, TemporaryPoint> &points);                                                ///< Set potential value

    public:
        Solver(const AbstractParameters *parameters, std::string filename); ///< Creates and initializes solver
        void solve();                                                       ///< Solves the problem
        void output();                                                      ///< Writes output to file
        ~Solver();                                                          ///< Destroys solver
    };
}