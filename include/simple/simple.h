/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */ 

#pragma once

#include <numsimulation/common.h>
#include <Eigen/Dense>
#include <fstream>
#include <vector>
#include <set>

using namespace numsimulation;

namespace simple
{
    struct Point;
    struct Face;
    struct Cell;
    struct TemporaryCell;

    /// Point. Cells and faces consist of points
    struct Point
    {
        //Constants
        Eigen::Vector2d coord;  ///< Point coordinate

        Point(Eigen::Vector2d coord);   ///< Creates point
    };

    /// Face. Face is a line between two points
    struct Face
    {
        /// Precalculated geometry data for each neighboring face
        struct FaceConstants
        {
            int neighbor_velocity_sign;         ///< Sign of velocity in neighboring cell (1 or -1)
            int neighbor_velocity_inwards_sign; ///< Sign of inwards velocity relative to velocity (1 or -1 or 0)
            double face_face_distance;          ///< Distance to neighboring face
            double border_length;               ///< Length of border between faces
        };

        //Logical information
        bool fixed_velocity = false;    ///< Face has fixed velocity
        Point *point[2];                ///< Face points
        std::vector<Cell*> cells;       ///< Neighboring cells (reverse pointers)
        std::vector<Face*> neighbors;   ///< Neighboring faces (reverse pointers)

        //Constants
        Eigen::Vector2d normal;         ///< Precalculated normal
        Eigen::Vector2d center;         ///< Precalculated center
        double length;                  ///< Precalculated length
        double cell_cell_distance;      ///< Precalculated distance between cells (corrected by normal)
        double area;                    ///< Precalculated area
        int pressure_sign;              ///< Precalculated sign of pressure (1 or -1)
        std::vector<FaceConstants> face_constants;   ///< Precalculated geometry data

        //Variables
        double velocity;                ///< Current most latest estimate of velocity V
        double velocity_estimate;       ///< Current estimate of velocity V*
        double new_velocity;            ///< New estimate of velocity V*, used in Jacobi algorithm

        //Cache
        double a0_tilde;                ///< a0_tilde = sum(a) coefficient, calculated during V* solver to be used in P' solver
        double c;                       ///< c = A/L/a0_tilde coefficient, precalculated for each P' solution 
    };

    /// Cell. Cell is the most important part of the simulation with a scalar value defined on each cell
    struct Cell
    {
        /// Precalculated geometry data for each face
        struct FaceConstants
        {
            int face_normal_sign;       //Sign of face normal (1 or -1)
            double face_cell_distance;  //Distance to center of face (corrected by normal)
            double cell_cell_distance;  //Distance to center of another cell (corrected by normal)
        };

        //Logical information
        bool fixed_pressure;            ///< Touches boundary and boundary is fixed-pressure
        bool fixed_e;                   ///< Cell is surrounded by fixed and/or edge faces, pressure not calculable, e not important
        std::vector<Point*> points;     ///< Cell points
        std::vector<Face*> faces;       ///< Cell faces
        std::vector<Cell*> neighbors;   ///< Cell faces

        //Constants
        double area;                                ///< Precalculated area
        Eigen::Vector2d center;                     ///< Precalculated center
        std::vector<FaceConstants> face_constants;  ///< Precalculated for each face

        //Variables
        double pressure;                ///< Current most latest estimate of pressure P
        double pressure_estimate;       ///< Current estimate of pressure P*
        double pressure_correction;     ///< Pressure correction P'
        double new_pressure_correction; ///< New pressure correction P', used in Jacobi algorithm
        double e;                       ///< e = sum(rho * L * v) coefficient, precalculated for each P' solution
        double c0_tilde;                ///< c_tilde = sum(c) coefficient, precalculated for each P' solution

        Cell(bool fixed_pressure, double area, Eigen::Vector2d center);  ///< Creates cell
    };

    /// Numerical method
    enum class Method
    {
        linear,
        upwind,
        hybrid,
        potential,
        exponential
    };

    /// Reaction on the error of linear solver
    enum class Reaction
    {
        error,
        warning,
        ignore
    };

    ///Boundary geometry and boundary conditions
    struct Boundary
    {
        Figure *figure;     ///< Boundary geometry
        bool fixed_pressure;///< Scalar is fixed
    };

    /// Parameters (parameter functions unimplemented)
    struct AbstractParameters
    {
        // Geometry parameters
        std::vector<Boundary> boundaries;             ///< List of boundaries

        // Grid parameters
        Eigen::Vector2d grid_origin;    ///< Grid origin, where the field probing begins
        double grid_size[2];            ///< Space between two neighboring points
        double area_threshold;          ///< Minimal area of the cell
        double length_threshold;        ///< Minimal length of the face to have its own velocity

        // Solver parameters
        Method method;                  ///< Numeric method
        double density;                 ///< Fluid density
        double viscosity;               ///< Fluid viscosity
        double step;                    ///< Time step
        bool exact;                     ///< Potential function is valid on entire field, used for testing
        //Iterative solver parameters
        double beta;                    ///< Undershooting/overshooting parameter
        double max_e;                   ///< Maximum e (flow in/out of cells)
        unsigned int  max_iteration;    ///< Maximum iteration
        //Linear solver for P' parameters
        double p_max_residual;          ///< Maximum residual for P' solver
        unsigned int p_max_iteration;   ///< Maximum iteration for P' solver
        bool p_gauss;                   ///< Defines whether Gauss-Seidel orJacobi algorithm is used for P' solver
        Reaction p_reaction;            ///< Reaction on P' solver failing to converge
        //Linear solver for V* parameters
        double v_max_residual;          ///< Maximum residual for V* solver
        unsigned int v_max_iteration;   ///< Maximum iteration for V* solver
        bool v_gauss;                   ///< Defines whether Gauss-Seidel orJacobi algorithm is used for V* solver
        Reaction v_reaction;            ///< Reaction on V* solver failing to converge

        AbstractParameters(numsimulation::ParameterReader &reader);                     ///< Reads abstract parameters from parameter reader
        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const = 0; ///< Velocity on field (must be implemented)
        virtual double pressure(Eigen::Vector2d coord, double time) const = 0;          ///< Pressure on field (must be implemented)
    };

    /// Solver of instationary flows with given pressure
    class Solver
    {
    private:
        const AbstractParameters *_parameters;
        const std::string _filename;
        std::ofstream _file;
        std::set<Point*> _points;
        std::set<Face*> _faces;
        std::set<Cell*> _cells;
        double _time = 0.0;

        void _add_first_cell(std::map<Position, TemporaryCell> &active);                                            ///< Adds first cell to active set
        void _add_all_cells(std::map<Position, TemporaryCell> &active, std::map<Position, TemporaryCell> &passive); ///< Searches and adds cells to passive set
        void _calculate_area(std::map<Position, TemporaryCell> &cells);                                             ///< Calculate area of the cells
        void _check_completeness(std::map<Position, TemporaryCell> &cells);                                         ///< Checks whether the cells are complete
        void _apply_failed_cells(std::map<Position, TemporaryCell> &cells);                                         ///< Apply condition of failed cells to healthy cells
        void _create_cells(std::map<Position, TemporaryCell> &cells);                                               ///< Create cells
        void _create_points(std::map<Position, TemporaryCell> &cells);                                              ///< Create points
        void _create_faces(std::map<Position, TemporaryCell> &cells);                                               ///< Create faces
        void _set_face_conditions(std::map<Position, TemporaryCell> &cells);                                        ///< Find and mark fixed velocity faces
        void _set_cell_conditions(std::map<Position, TemporaryCell> &cells);                                        ///< Find and mark fixed e cells
        void _precalculate_faces();                                                                                 ///< Precalculate face constants
        void _interconnect_faces(std::map<Position, TemporaryCell> &cells);                                         ///< Setup face->cell and face->face pointers
        void _precalculate_faces_cells();                                                                           ///< Precalculate face->cells constants
        void _precalculate_cells_faces();                                                                           ///< Precalculate cell->face constants
        void _setup_pressure();                                                                                     ///< Setup pressure on all cells
        void _setup_velocity();                                                                                     ///< Setup velocity on all faces
        
        double _a(double pe) const;                                                                                 ///< Calculates A(Pe)
        void _setup_fixed_pressure();                                                                               ///< Setup pressure on all cells
        void _setup_fixed_velocity();                                                                               ///< Setup velocity on all fixed faces
        void _solve_velocity();                                                                                     ///< Solves linear system for V
        double _solve_pressure_correction();                                                                        ///< Solves linear system for P', returns e
        void _set_correct_pressure();                                                                               ///< Updates P = P* + P'
        void _set_correct_velocity();                                                                               ///< Updates V = V* + V'
        void _set_pressure_estimate();                                                                              ///< Updates V* = V
        void _set_velocity_estimate();                                                                              ///< Updates P* = P
        void _calculate_a0_tilde();                                                                                 ///< Calculates a0_tilde

    public:
        Solver(const AbstractParameters *parameters, std::string filename); ///< Creates solver
        void step();                                                        ///< Makes a simulation step
        void output();                                                      ///< Outputs current state
        double time() const;                                                ///< Gets current simulation time
        ~Solver();                                                          ///< Destroys solver
    };
}