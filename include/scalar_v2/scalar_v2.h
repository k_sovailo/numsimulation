/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */ 

#pragma once
#include <vector>
#include <set>
#include <Eigen/Dense>
#include <numsimulation/common.h>
#include <fstream>

using namespace numsimulation;

namespace scalar_v2
{
    struct Point;
    struct Face;
    struct Cell;
    struct TemporaryCell;

    /// Point. Cells and faces consist of points
    struct Point
    {
        //Constants
        Eigen::Vector2d coord;  ///< Point coordinate

        Point(Eigen::Vector2d coord);   ///< Creates point
    };

    /// Face. Face is a line between two points
    struct Face
    {
        //Logical information
        Point *point[2];        ///< Face points

        //Constants
        Eigen::Vector2d center; ///< Precalculated center
        Eigen::Vector2d normal; ///< Precalculated normal
        double length;          ///< Precalculated length
    };

    /// Cell. Cell is the most important part of the simulation with a scalar value defined on each cell
    struct Cell
    {
        /// Precalculated geometry data for each neighboring cell
        struct FaceConstants
        {
            int face_normal_sign;       //Sign of face normal (1 or -1)
            double cell_cell_distance;  //Distance to center of another cell (corrected by normal)
        };

        //Logical information
        bool fixed_scalar;              ///< Touches boundary and boundary is fixed
        std::vector<Point*> points;     ///< Cell points
        std::vector<Face*> faces;       ///< Cell faces
        std::vector<Cell*> neighbors;   ///< Cell faces

        //Constants
        double area;                                ///< Precalculated area
        Eigen::Vector2d center;                     ///< Precalculated center
        std::vector<FaceConstants> face_constants;  ///< Precalculated constants for each face

        //Variables
        double scalar;                              ///< Scalar value
        double new_scalar;                          ///< New scalar value (internal usage)

        Cell(bool fixed_scalar, double area, Eigen::Vector2d center);  ///< Creates cell
    };

    /// Numerical method
    enum class Method
    {
        linear,
        upwind,
        hybrid,
        potential,
        exponential
    };

    ///Boundary geometry and boundary conditions
    struct Boundary
    {
        Figure *figure;     ///< Boundary geometry
        bool fixed_scalar;  ///< Scalar is fixed
    };

    /// Parameters (parameter functions unimplemented)
    struct AbstractParameters
    {
        // Geometry parameters
        std::vector<Boundary> boundaries;   ///< List of boundaries

        // Grid parameters
        Eigen::Vector2d grid_origin;///< Grid origin, where the field probing begins
        GridType grid_type;         ///< Grid type
        double grid_size[2];        ///< Space between two neighboring points
        double area_threshold;      ///< Minimal area of the cell

        // Solver parameters
        Method method;              ///< Numeric method
        double density;             ///< Fluid density
        double diffusion;           ///< Diffusion coefficient
        double step;                ///< Time step
        bool exact;                 ///< Potential function is valid on entire field, used for testing

        AbstractParameters(numsimulation::ParameterReader &reader);                     ///< Reads abstract parameters from parameter reader
        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const = 0; ///< Velocity on field (must be implemented)
        virtual double scalar(Eigen::Vector2d coord, double time) const = 0;            ///< Scalar on field (must be implemented)
    };

    /// More efficient solver of instationary flows with given velocity
    class Solver
    {
    private:
        const AbstractParameters *_parameters;
        const std::string _filename;
        std::ofstream _file;
        std::set<Point*> _points;
        std::set<Face*> _faces;
        std::set<Cell*> _cells;
        double _time = 0.0;

        void _add_first_cell(std::map<Position, TemporaryCell> &active);                                            ///< Adds first cell to active set
        void _add_all_cells(std::map<Position, TemporaryCell> &active, std::map<Position, TemporaryCell> &passive); ///< Searches and adds cells to passive set
        void _calculate_area(std::map<Position, TemporaryCell> &cells);                                             ///< Calculate area of the cells
        void _apply_failed_cells(std::map<Position, TemporaryCell> &cells);                                         ///< Apply condition of failed cells to healthy cells
        void _create_cells(std::map<Position, TemporaryCell> &cells);                                               ///< Create cells
        void _create_points(std::map<Position, TemporaryCell> &cells);                                              ///< Create points
        void _create_faces(std::map<Position, TemporaryCell> &cells);                                               ///< Create faces
        void _precalculate_faces();                                                                                 ///< Precalculate face constants
        void _precalculate_cells_faces();                                                                           ///< Precalculate cell->face constants
        void _setup_scalar();                                                                                       ///< Setup scalar on all cells
        void _setup_fixed_scalar();                                                                                 ///< Setup scalar on fixed cells
        double _a(double pe) const;                                                                                 ///< Calculates A(Pe)

    public:
        Solver(const AbstractParameters *parameters, std::string filename); ///< Creates solver
        void step();                                                        ///< Makes a simulation step
        void output();                                                      ///< Outputs current state
        double time() const;                                                ///< Gets current simulation time
        ~Solver();                                                          ///< Destroys solver
    };
}