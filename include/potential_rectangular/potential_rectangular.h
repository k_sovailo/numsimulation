/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */ 

#pragma once

#include <numsimulation/common.h>
#include <vector>
#include <string>
#include <Eigen/Dense>

namespace potential_rectangular
{
    /// Smallest element of the simulation. It is a point with potential defined on it
    struct Point
    {
        double potential;           ///< Potential
        double new_potential;       ///< New potential, used in Jacobi algorithm
    };
    
    /// Parameters of the simulation
    struct AbstractParameters
    {
        // Geometry parameters
        double min[2];              ///< Lower boundaries of the simulation field
        double max[2];              ///< Upper boundaries of the simulation field

        // Grid parameters
        unsigned int n[2];          ///< Number of points
        
        // Solver parameters
        unsigned int max_iteration; ///< Maximum iteration
        double max_residual;        ///< Maximum residual
        double beta;                ///< Undershooting/overshooting parameter
        bool gauss;                 ///< Defines whether Gauss-Seidel orJacobi algorithm is used
        bool exact;                 ///< Potential function is valid on entire field, used for testing

        AbstractParameters(numsimulation::ParameterReader &reader); ///< Reads abstract parameters from parameter reader
        virtual double potential(Eigen::Vector2d coord) const = 0;  ///< Potential function (must be implemented)
    };

    /// Solves simple potential flow problem on rectangular field
    class Solver
    {
    private:
        const AbstractParameters *_parameters;      ///< Simulation parameters
        const std::string _filename;                ///< Filename to save results
        std::vector<std::vector<Point>> _points;    ///< Simulation field

        Eigen::Vector2d _get_coord(unsigned int xi, unsigned int yi) const; ///< Get point coordinates based on it's number

    public:
        Solver(const AbstractParameters *parameters, std::string filename); ///< Creates and initializes solver
        void solve();                                                       ///< Solves the problem
        void output();                                                      ///< Writes output to file
    };
}