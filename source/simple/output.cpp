/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple/simple.h>

void simple::Solver::output()
{
    _file << "T " << _time << "\n";

    //Write pressure data
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        _file << "X " << cell->center(0) << " Y " << cell->center(1) << " P " << cell->pressure;
        if (_parameters->exact) _file << " EP " << _parameters->pressure(cell->center, _time) << "\n";
        _file << "\n";
    }

    //Write velocity data
    for (std::set<Face*>::iterator iface = _faces.begin(); iface != _faces.end(); iface++)
    {
        Face *face = *iface;
        _file << "X " << face->center(0) << " Y " << face->center(1);
        if (abs(face->normal(0)) > abs(face->normal(1)))
        {
            //Horizontal normal, vertical face
            _file << " VX " << (face->normal * face->velocity).x();
            if (_parameters->exact) _file << " EVX " << _parameters->velocity(face->center, _time).x();
            _file << "\n";
        }
        else
        {
            //Vertical normal, horizontal face
            _file << " VY " << (face->normal * face->velocity).y();
            if (_parameters->exact) _file << " EVY " << _parameters->velocity(face->center, _time).y();
            _file << "\n";
        }
    }
}