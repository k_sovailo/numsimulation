/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple/simple.h>

double simple::Solver::time() const
{
    return _time;
}