/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple/simple.h>

simple::AbstractParameters::AbstractParameters(numsimulation::ParameterReader &reader)
{
    //Grid parameters
    grid_size[0] = reader.get_real("GRID_SIZE_X"); grid_size[1] = reader.get_real("GRID_SIZE_Y");
    area_threshold = reader.get_real("AREA_THRESHOLD");

    // Solver parameters
    if (reader.get_string("METHOD") == "LINEAR") method = Method::linear;
    else if (reader.get_string("METHOD") == "UPWIND") method = Method::upwind;
    else if (reader.get_string("METHOD") == "HYBRID") method = Method::hybrid;
    else if (reader.get_string("METHOD") == "POTENTIAL") method = Method::potential;
    else if (reader.get_string("METHOD") == "EXPONENTIAL") method = Method::exponential;
    else throw std::runtime_error("_main(): Unrecognized numerical method");
    density = reader.get_real("DENSITY");
    viscosity = reader.get_real("VISCOSITY");
    step = reader.get_real("STEP");
    exact = false;
    //Iterative solver parameters
    beta = reader.get_real("BETA");
    max_iteration = reader.get_real("MAX_ITERATION");
    max_e = reader.get_real("MAX_E");
    //Linear solver for P' parameters
    p_max_residual = reader.get_real("P_MAX_RESIDUAL");
    p_max_iteration = reader.get_integer("P_MAX_ITERATION");
    if (reader.get_string("P_SOLVER") == "GAUSS") p_gauss = true;
    else if (reader.get_string("P_SOLVER") == "JACOBI") p_gauss = false;
    else throw std::runtime_error("simple::AbstractParameters::AbstractParameters(): unrecognized solver");
    if (reader.get_string("P_REACTION") == "IGNORE") p_reaction = Reaction::ignore;
    else if (reader.get_string("P_REACTION") == "WARNING") p_reaction = Reaction::warning;
    else if (reader.get_string("P_REACTION") == "ERROR") p_reaction = Reaction::error;
    else throw std::runtime_error("simple::AbstractParameters::AbstractParameters(): unrecognized reaction");
    //Linear solver for V* parameters
    v_max_residual = reader.get_real("V_MAX_RESIDUAL");
    v_max_iteration = reader.get_integer("V_MAX_ITERATION");
    if (reader.get_string("V_SOLVER") == "GAUSS") v_gauss = true;
    else if (reader.get_string("V_SOLVER") == "JACOBI") v_gauss = false;
    else throw std::runtime_error("simple::AbstractParameters::AbstractParameters(): unrecognized solver");
    if (reader.get_string("V_REACTION") == "IGNORE") v_reaction = Reaction::ignore;
    else if (reader.get_string("V_REACTION") == "WARNING") v_reaction = Reaction::warning;
    else if (reader.get_string("V_REACTION") == "ERROR") v_reaction = Reaction::error;
    else throw std::runtime_error("simple::AbstractParameters::AbstractParameters(): unrecognized reaction");
}