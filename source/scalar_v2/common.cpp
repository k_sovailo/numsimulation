/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar_v2/scalar_v2.h>

double scalar_v2::Solver::time() const
{
    return _time;
}