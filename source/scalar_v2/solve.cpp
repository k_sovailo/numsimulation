/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar_v2/scalar_v2.h>
#include <iostream>

double scalar_v2::Solver::_a(double pe) const
{
    switch (_parameters->method)
    {
        case Method::linear: return 1 - 0.5 * abs(pe);
        case Method::upwind: return 1;
        case Method::hybrid: return std::max(0.0, 1 - 0.5 * abs(pe));
        case Method::potential: pe = 1 - 0.1 * abs(pe); return std::max(0.0, pe*pe*pe*pe*pe);
        case Method::exponential: return abs(pe) / exp(abs(pe) - 1);
    }
    return 0;
}

void scalar_v2::Solver::_setup_fixed_scalar()
{
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        if (cell->fixed_scalar) cell->scalar = _parameters->scalar(cell->center, _time);
    }
}

void scalar_v2::Solver::step()
{
    _setup_fixed_scalar();

    //Processing
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        if (cell->fixed_scalar) continue; //Skip fixed cells
        
        double a0 = _parameters->density * cell->area / _parameters->step;  //a0 becomes a0_tilde till the end of the iteration
        double b = a0 * cell->scalar;                                       //b becomes a0_tilde*next_scalar till the end of the iteration
        for (unsigned int n = 0; n < cell->faces.size(); n++)
        {
            //Precalculating
            if (cell->neighbors[n] == nullptr) continue; //No cell on other side
            
            const Eigen::Vector2d face_normal = cell->face_constants[n].face_normal_sign * cell->faces[n]->normal;
            const double face_length = cell->faces[n]->length;
            const double cell_cell_distance = cell->face_constants[n].cell_cell_distance;
            const double neighbor_cell_velocity = (_parameters->velocity(cell->neighbors[n]->center, _time)).dot(face_normal); //incoming flow

            //Kernel
            const double d = _parameters->diffusion / cell_cell_distance;
            const double pe = _parameters->density * neighbor_cell_velocity * cell_cell_distance / _parameters->diffusion;
            const double f = neighbor_cell_velocity * _parameters->density;
            const double a = d * face_length * _a(pe) + std::max(0.0, f * face_length);
            a0 += a;
            b += a * cell->neighbors[n]->scalar;
        }
        cell->new_scalar = b / a0;
    }

    //Postprocessing
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        if (!cell->fixed_scalar) cell->scalar = cell->new_scalar;
    }
    
    //Progressing time
    std::streamsize precision = std::cout.precision(4);
    std::cout << "Time step " << _time << " --> " << _time + _parameters->step << " done\n";
    std::cout.precision(precision);
    _time += _parameters->step;
}