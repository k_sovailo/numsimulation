/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple_partial/simple_partial.h>

double simple_partial::Solver::time() const
{
    return _time;
}