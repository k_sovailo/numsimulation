/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple_partial/simple_partial.h>
#include <iostream>

double simple_partial::Solver::_a(double pe) const
{
    switch (_parameters->method)
    {
        case Method::linear: return 1 - 0.5 * abs(pe);
        case Method::upwind: return 1;
        case Method::hybrid: return std::max(0.0, 1 - 0.5 * abs(pe));
        case Method::potential: pe = 1 - 0.1 * abs(pe); return std::max(0.0, pe*pe*pe*pe*pe);
        case Method::exponential: return abs(pe) / exp(abs(pe) - 1);
    }
    return 0;
}

void simple_partial::Solver::step()
{
    //Settting up pressure
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        cell->pressure = _parameters->pressure(cell->center, _time);
    }

    //Setting up fixed faces
    for (std::set<Face*>::iterator iface = _faces.begin(); iface != _faces.end(); iface++)
    {
        Face *face = *iface;
        if (face->cells.size() < 2) face->velocity = _parameters->velocity(face->center, _time).dot(face->normal);
    }

    //Processing
    for (std::set<Face*>::iterator iface = _faces.begin(); iface != _faces.end(); iface++)
    {
        Face *face = *iface;
        if (face->cells.size() < 2) continue; //Skip fixed cells
        
        double a0 = _parameters->density * face->area / _parameters->step;  //a0 becomes a0_tilde till the end of the iteration
        double b = a0 * face->velocity;                                     //b becomes a0_tilde*next_velocity till the end of the iteration
        b -= face->pressure_sign * (face->cells[1]->pressure - face->cells[0]->pressure) * face->area / face->cell_cell_distance;
        for (unsigned int n = 0; n < face->neighbors.size(); n++)
        {
            //Precalculating
            const double neighbor_velocity = face->face_constants[n].neighbor_velocity_sign * face->neighbors[n]->velocity;
            const double neighbor_velocity_inwards = face->face_constants[n].neighbor_velocity_inwards_sign * neighbor_velocity;
            const double face_face_distance = face->face_constants[n].face_face_distance;
            const double border_length = face->face_constants[n].border_length;

            //Kernel
            const double d = _parameters->viscosity / face_face_distance;
            const double pe = _parameters->density * neighbor_velocity_inwards * face_face_distance / _parameters->viscosity;
            const double f = neighbor_velocity_inwards * _parameters->density;
            const double a = d * border_length * _a(pe) + std::max(0.0, f * border_length);
            a0 += a;
            b += a * neighbor_velocity;
        }
        face->new_velocity = b / a0;
    }

    //Postprocessing
    for (std::set<Face*>::iterator iface = _faces.begin(); iface != _faces.end(); iface++)
    {
        Face *face = *iface;
        if (face->cells.size() < 2) continue;
        face->velocity = face->new_velocity;
    }

    //Progressing time
    std::streamsize precision = std::cout.precision(4);
    std::cout << "Time step " << _time << " --> " << _time + _parameters->step << " done\n";
    std::cout.precision(precision);
    _time += _parameters->step;
}