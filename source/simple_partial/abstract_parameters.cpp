/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple_partial/simple_partial.h>

simple_partial::AbstractParameters::AbstractParameters(numsimulation::ParameterReader &reader)
{
    //Grid parameters
    grid_size[0] = reader.get_real("GRID_SIZE_X"); grid_size[1] = reader.get_real("GRID_SIZE_Y");
    area_threshold = reader.get_real("AREA_THRESHOLD");

    // Solver parameters
    if (reader.get_string("METHOD") == "LINEAR") method = Method::linear;
    else if (reader.get_string("METHOD") == "UPWIND") method = Method::upwind;
    else if (reader.get_string("METHOD") == "HYBRID") method = Method::hybrid;
    else if (reader.get_string("METHOD") == "POTENTIAL") method = Method::potential;
    else if (reader.get_string("METHOD") == "EXPONENTIAL") method = Method::exponential;
    else throw std::runtime_error("_main(): Unrecognized numerical method");
    density = reader.get_real("DENSITY");
    viscosity = reader.get_real("VISCOSITY");
    step = reader.get_real("STEP");

    exact = false;
}