/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential/potential.h>
#include <fstream>

void potential::Solver::output()
{
    //Open file
    std::ofstream file(_filename, std::ios::binary);
    if (!file.good()) throw std::runtime_error("potential_warped::Solver::_output(): Cannot open file");
    
    //Write header
    double min[2] = { std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity() };
    double max[2] = { -std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity() };
    for (std::set<Point*>::iterator ipoint = _points.begin(); ipoint != _points.end(); ipoint++)
    {
        Point *point = *ipoint;
        if (point->coord(0) < min[0]) min[0] = point->coord(0);
        if (point->coord(1) < min[1]) min[1] = point->coord(1);
        if (point->coord(0) > max[0]) max[0] = point->coord(0);
        if (point->coord(1) > max[1]) max[1] = point->coord(1);
    }
    file << "NX " << (unsigned int)((max[0] - min[0]) / _parameters->grid_size[0]) << " NY " << (unsigned int)((max[1] - min[1]) / _parameters->grid_size[1]) << '\n';
    file << "T 0\n";

    //Write data
    for (std::set<Point*>::iterator ipoint = _points.begin(); ipoint != _points.end(); ipoint++)
    {
        Point *point = *ipoint;
        file << "X " << point->coord(0) << " Y " << point->coord(1) << " H " << point->potential;
        if (_parameters->exact) file << " EH " << _parameters->potential(point->coord);
        file << '\n';
    }
}