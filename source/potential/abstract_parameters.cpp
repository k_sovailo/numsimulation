/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential/potential.h>

potential::AbstractParameters::AbstractParameters(numsimulation::ParameterReader &reader)
{
    //Grid parameters
    if (reader.get_string("GRID") == "TRIANGULAR") grid_type = GridType::triangular;
    else if (reader.get_string("GRID") == "SQUARE") grid_type = GridType::square;
    else if (reader.get_string("GRID") == "HEXAGONAL") grid_type = GridType::hexagonal;
    else throw std::runtime_error("potential::AbstractParameters::AbstractParameters(): Unrecognized grid type");
    grid_size[0] = reader.get_real("GRID_SIZE_X"); grid_size[1] = reader.get_real("GRID_SIZE_Y");

    // Solver parameters
    max_iteration = reader.get_integer("MAX_ITERATION");
    max_residual = reader.get_real("MAX_RESIDUAL");
    beta = reader.get_real("BETA");
    if (reader.get_string("SOLVER") == "GAUSS") gauss = true;
    else if (reader.get_string("SOLVER") == "JACOBI") gauss = false;
    else throw std::runtime_error("potential::AbstractParameters::AbstractParameters(): unrecognized solver");

    exact = false;
}