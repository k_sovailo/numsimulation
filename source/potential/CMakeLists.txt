# This file is a part of Numsimulation project
# Developed as part of Numerical Flow Simulation course
# Author: Kyrylo Sovailo

project(potential)

# Demonstration
add_library(potential SHARED abstract_parameters.cpp solve.cpp output.cpp potential.cpp)
target_link_libraries(potential PUBLIC common)