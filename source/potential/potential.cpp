/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential/potential.h>

/*
    Points are divided into active, passive and unreached
    Unreached points have not yet been reached by the algorithm
    Active and passive points are both "reached" points, they are divided in two categories for optimization reasons
    The algorithm actively searches around "active" point, but does not search around "passive" points

    Because adding/removing elements from set cannot be done while iterating through it, there are sets like "to_be_active", etc.
    
    In this version of the algorithm active points with guaranty become passive after one iteration, so no checks are done
*/

namespace potential
{
    struct TemporaryPoint
    {
        Eigen::Vector2d normal; //Intersection potential, valid if not free
        double potential;       //Intersection normal, valid if not free
        BoundaryType typ = BoundaryType::free;

        Point *point = nullptr;
    };
}

potential::Point::Point(Eigen::Vector2d coord, BoundaryType typ) : coord(coord), typ(typ) {}

void potential::Solver::_add_first_point(std::map<Position, TemporaryPoint> &active)
{
    active.insert({Position(), TemporaryPoint()});
}

void potential::Solver::_add_all_points(std::map<Position, TemporaryPoint> &active, std::map<Position, TemporaryPoint> &passive)
{
    while (!active.empty())
    {
        //Iterate through active, add new points to "to_be_active"
        std::map<Position, TemporaryPoint> to_be_active;
        for (std::map<Position, TemporaryPoint>::iterator point = active.begin(); point != active.end(); point++)
        {
            Eigen::Vector2d active_coord = get_center(point->first, _parameters->grid_type, _parameters->grid_origin, _parameters->grid_size);
            for (unsigned int f = 0; f < get_shape(_parameters->grid_type); f++)     //Look on every neighbor
            {
                const Position neighbor = get_face_neighbor(point->first, _parameters->grid_type, f).position;
                if (passive.find(neighbor) != passive.end()) continue;              //Already passive, skip
                if (active.find(neighbor) != active.end()) continue;                //Already active, skip
                const Eigen::Vector2d to_be_active_coord = get_center(neighbor, _parameters->grid_type, _parameters->grid_origin, _parameters->grid_size);
                Intersection intersection;
                for (std::vector<Boundary>::const_iterator boundary = _parameters->boundaries.begin(); boundary != _parameters->boundaries.end(); boundary++) //Probe every boundary
                {
                    intersection = boundary->figure->intersection(active_coord, to_be_active_coord);
                    if (intersection.valid) //Boundary found, remember conditions
                    {
                        point->second.typ = boundary->typ;
                        point->second.potential = _parameters->potential(intersection.coord);
                        point->second.normal = intersection.normal;
                        break;
                    }
                }
                if (!intersection.valid)  //Boundary not found, create point
                {
                    if (to_be_active.find(neighbor) == to_be_active.end()) to_be_active.insert({neighbor, TemporaryPoint()});
                }
            }
        }
        passive.insert(active.begin(), active.end());   //All active are now passive, no checks needed
        active = to_be_active;                          //All to_be_active are now active
    }
}

void potential::Solver::_create_points(std::map<Position, TemporaryPoint> &points)
{
    for (std::map<Position, TemporaryPoint>::iterator point = points.begin(); point != points.end(); point++)
    {
        _points.insert(point->second.point = new Point(get_center(point->first, _parameters->grid_type, _parameters->grid_origin, _parameters->grid_size), point->second.typ));
    }
}

void potential::Solver::_precalculate_coefficients(std::map<Position, TemporaryPoint> &points)
{
    for (std::map<Position, TemporaryPoint>::iterator point = points.begin(); point != points.end(); point++)
    {
        //B * k = x
        //B is [ 1, xn-x0, yn-y0, (xn-x0)^2/2, (yn-y0)^2/2, (xn-x0)(yn-y0); ... ]
        //k is [ p0, dp/dx, dp/dy, ddp/ddx, ddp/ddy, ddp/dx/dy ]
        //x is [ p0, p1, p2, ... ]
        //B+  is B+  * x = k
        Point *p = point->second.point; //Shortcut
        Eigen::Matrix<double, Eigen::Dynamic, 6> B(p->neighbors.size()+1, 6);
        B(0, 0) = 1;
        B.block<1, 5>(0, 1).setZero();
        for (unsigned int j = 0; j < p->neighbors.size(); j++)
        {
            B(j+1, 0) = 1;
            B(j+1, 1) = p->neighbors[j]->coord.x() - p->coord.x();
            B(j+1, 2) = p->neighbors[j]->coord.y() - p->coord.y();
            B(j+1, 3) = sqr(p->neighbors[j]->coord.x() - p->coord.x()) / 2;
            B(j+1, 4) = sqr(p->neighbors[j]->coord.y() - p->coord.y()) / 2;
            B(j+1, 5) = (p->neighbors[j]->coord.x() - p->coord.x()) * (p->neighbors[j]->coord.y() - p->coord.y());
        }
        Eigen::Matrix<double, 6, Eigen::Dynamic> Bp = B.transpose() * (B * B.transpose()).inverse();
        if (p->typ == BoundaryType::free)
        {
            p->b = Bp.row(3) + Bp.row(4); //Add ddp/ddx and ddp/ddy
        }
        else if (p->typ == BoundaryType::wall)
        {
            p->b = point->second.normal.transpose() * Bp.middleRows(1, 2); //Multiply -normal by (dp/dx, dp/dy)
        }
    }
}

void potential::Solver::_interconnect_points(std::map<Position, TemporaryPoint> &points)
{
    for (std::map<Position, TemporaryPoint>::iterator point = points.begin(); point != points.end(); point++)
    {
        for (unsigned int f = 0; f < get_shape(_parameters->grid_type); f++)
        {
            const Position neighbor = get_face_neighbor(point->first, _parameters->grid_type, f).position;
            std::map<Position, TemporaryPoint>::iterator find = points.find(neighbor);
            if (find != points.end()) point->second.point->neighbors.push_back(find->second.point);
        }
    }
}

void potential::Solver::_set_potential(std::map<Position, TemporaryPoint> &points)
{
    for (std::map<Position, TemporaryPoint>::iterator point = points.begin(); point != points.end(); point++)
    {
        if (point->second.typ == BoundaryType::fixed) point->second.point->potential = point->second.potential;
        else point->second.point->potential = 0;
    }
}

potential::Solver::Solver(const AbstractParameters *parameters, std::string filename) : _parameters(parameters), _filename(filename)
{
    std::map<Position, TemporaryPoint> passive, active;
    _add_first_point(active);
    _add_all_points(active, passive);
    _create_points(passive);
    _interconnect_points(passive);
    _precalculate_coefficients(passive);
    _set_potential(passive);
}

potential::Solver::~Solver()
{
    for (std::set<Point*>::iterator ipoint = _points.begin(); ipoint != _points.end(); ipoint++)
    {
        delete (*ipoint);
    }
}