/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential/potential.h>
#include <iostream>

void potential::Solver::solve()
{
    //Iterate through all
    double previous_residual = std::numeric_limits<double>::infinity();
    unsigned int fail_count = 0;
    unsigned int iteration = 0;
    while (true)
    {
        iteration++;
        double max_residual = -std::numeric_limits<double>::infinity();

        //Main calculation
        for (std::set<Point*>::iterator ipoint = _points.begin(); ipoint != _points.end(); ipoint++)
        {
            Point *point = *ipoint;
            if (point->typ == BoundaryType::fixed) continue; //Skip fixed
            
            //Filling x
            Eigen::VectorXd x(point->neighbors.size()+1);
            x(0) = point->potential;
            for (unsigned int j = 0; j < point->neighbors.size(); j++) x(j+1) = point->neighbors[j]->potential;
            //Calculating coefficients
            const double multiplied_without_center = point->b.tail(point->neighbors.size()).dot(x.tail(point->neighbors.size()));
            const double center_coefficient = point->b(0);
            const double multiplied_only_center = center_coefficient * x(0);
            //Residual
            const double residual = multiplied_without_center + multiplied_only_center;
            if (abs(residual) > max_residual) max_residual = abs(residual);
            //New potential
            double new_potential = -multiplied_without_center / center_coefficient;
            new_potential = _parameters->beta * new_potential + (1.0 - _parameters->beta) * x(0);
            (_parameters->gauss ? point->potential : point->new_potential) = new_potential;
        }

        //Applying new_potential (Jacobi algorithm)
        if (!_parameters->gauss)
        {
            for (std::set<Point*>::iterator ipoint = _points.begin(); ipoint != _points.end(); ipoint++)
            {
                Point *point = *ipoint;
                if (point->typ == BoundaryType::fixed) continue; //Skip fixed

                point->potential = point->new_potential;
            }
        }

        //Logics
        if (max_residual < _parameters->max_residual)
        {
            std::cout << "Done in " << iteration << " iterations\n";
            break;
        }
        if (iteration > _parameters->max_iteration)
        {
            throw std::runtime_error("Number of iterations exceeded");
        }
        if (max_residual > previous_residual)
        {
            if (fail_count++ > 100) throw std::runtime_error("Residual does not converge");
        }
        previous_residual = max_residual;
    }
} 