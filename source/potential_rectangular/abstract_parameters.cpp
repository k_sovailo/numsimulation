/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_rectangular/potential_rectangular.h>

potential_rectangular::AbstractParameters::AbstractParameters(numsimulation::ParameterReader &reader)
{
    // Geometry parameters
    min[0] = reader.get_real("XMIN"); min[1] = reader.get_real("YMIN");
    max[0] = reader.get_real("XMAX"); max[1] = reader.get_real("YMAX");

    // Grid parameters
    n[0] = reader.get_integer("NX"); n[1] = reader.get_integer("NY");
    
    // Solver parameters
    max_iteration = reader.get_integer("MAX_ITERATION");
    max_residual = reader.get_real("MAX_RESIDUAL");
    beta = reader.get_real("BETA");
    if (reader.get_string("SOLVER") == "GAUSS") gauss = true;
    else if (reader.get_string("SOLVER") == "JACOBI") gauss = false;
    else throw std::runtime_error("potential_rectangular::AbstractParameters::AbstractParameters(): unrecognized solver");

    exact = false;
}