/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_rectangular/potential_rectangular.h>
#include <fstream>

void potential_rectangular::Solver::output()
{
    //Open file
    std::ofstream file(_filename, std::ios::binary);
    if (!file.good()) throw std::runtime_error("potential_rectangular::Solver::_output(): Cannot open file");
    
    //Write header
    file << "NX " << _parameters->n[0] << " NY " << _parameters->n[1] << '\n';
    file << "T 0\n";
    file << "X"; for (unsigned int xi = 0; xi < _parameters->n[0]; xi++) file << " " << _get_coord(xi, 0).x(); file << '\n';
    file << "Y"; for (unsigned int yi = _parameters->n[1] - 1; yi != (unsigned int)-1; yi--) file << " " << _get_coord(0, yi).y(); file << '\n';

    //Write data
    file << "H\n";
    for (unsigned int yi = _parameters->n[1] - 1; yi != (unsigned int)-1; yi--)
    {
        for (unsigned int xi = 0; xi < _parameters->n[0]; xi++) file << _points[xi][yi].potential << " ";
        file << '\n';
    }
    file << '\n';

    //Write exact data
    if (_parameters->exact)
    {
        //Write exact data
        file << "EH\n";
        for (unsigned int yi = _parameters->n[1] - 1; yi != (unsigned int)-1; yi--)
        {
            for (unsigned int xi = 0; xi < _parameters->n[0]; xi++) file << _parameters->potential(_get_coord(xi, yi)) << " ";
            file << '\n';
        }
        file << '\n';
    }
}