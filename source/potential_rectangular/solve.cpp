/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_rectangular/potential_rectangular.h>
#include <numsimulation/common.h>
#include <iostream>
using namespace numsimulation;

void potential_rectangular::Solver::solve()
{
    const double dx2 = sqr((_parameters->max[0] - _parameters->min[0]) / (_parameters->n[0] - 1));
    const double dy2 = sqr((_parameters->max[1] - _parameters->min[1]) / (_parameters->n[1] - 1));
    double previous_residual = std::numeric_limits<double>::infinity();
    unsigned int fail_count = 0;
    unsigned int iteration = 0;
    while (true)
    {
        iteration++;
        double max_residual = -std::numeric_limits<double>::infinity();

        //Main calculation
        for (unsigned int xi = 1; xi < _parameters->n[0] - 1; xi++)
        {
            for (unsigned int yi = 1; yi < _parameters->n[1] - 1; yi++)
            {
                //A*x without center
                double multiplied_without_center = (
                    (_points[xi-1][yi].potential + _points[xi+1][yi].potential) / dx2 +
                    (_points[xi][yi-1].potential + _points[xi][yi+1].potential) / dy2
                );
                //A in the center
                const double center_coefficient = (
                    - 2 / dx2
                    - 2 / dy2
                );
                //A*x only center
                const double multiplied_only_center = center_coefficient * _points[xi][yi].potential;

                // Residual A*x-b
                const double residual = multiplied_without_center + multiplied_only_center;
                if (abs(residual) > max_residual) max_residual = abs(residual);

                // New potential (Gauss-Seidel or Jacobi algorithm)
                double new_potential = -multiplied_without_center / center_coefficient;
                new_potential = _parameters->beta * new_potential + (1.0 - _parameters->beta) * _points[xi][yi].potential;
                (_parameters->gauss ? _points[xi][yi].potential : _points[xi][yi].new_potential) = new_potential;
            }
        }

        //Applying new_potential (Jacobi algorithm)
        if (!_parameters->gauss)
        {
            for (unsigned int xi = 1; xi < _parameters->n[0] - 1; xi++)
            {
                for (unsigned int yi = 1; yi < _parameters->n[1] - 1; yi++)
                {
                    _points[xi][yi].potential = _points[xi][yi].new_potential;
                }
            }
        }

        //Logics
        if (max_residual < _parameters->max_residual)
        {
            std::cout << "Done in " << iteration << " iterations\n";
            break;
        }
        if (iteration > _parameters->max_iteration)
        {
            throw std::runtime_error("Number of iterations exceeded");
        }
        if (max_residual > previous_residual)
        {
            if (fail_count++ > 100) throw std::runtime_error("Residual does not converge");
        }
        previous_residual = max_residual;
    }
}
