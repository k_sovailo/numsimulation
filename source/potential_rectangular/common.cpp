/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_rectangular/potential_rectangular.h>

Eigen::Vector2d potential_rectangular::Solver::_get_coord(unsigned int xi, unsigned int yi) const
{
    return Eigen::Vector2d(
        (_parameters->min[0] * (_parameters->n[0] - xi - 1) + _parameters->max[0] * xi) / (_parameters->n[0] - 1),
        (_parameters->min[1] * (_parameters->n[1] - yi - 1) + _parameters->max[1] * yi) / (_parameters->n[1] - 1)
    );
}