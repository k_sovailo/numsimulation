/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_rectangular/potential_rectangular.h>

potential_rectangular::Solver::Solver(const AbstractParameters *parameters, std::string filename) : _parameters(parameters), _filename(filename)
{
    //Allocate
    _points.resize(parameters->n[0]);
    for (unsigned int xi = 0; xi < parameters->n[0]; xi++) _points[xi].resize(parameters->n[1]);

    //Setup
    for (unsigned int xi = 0; xi < parameters->n[0]; xi++)
    {
        for (unsigned int yi = 0; yi < parameters->n[1]; yi++) _points[xi][yi].potential = 0;
    }
    for (unsigned int xi = 0; xi < parameters->n[0]; xi++)
    {
        _points[xi][0].potential = parameters->potential(_get_coord(xi, 0));
        _points[xi][parameters->n[1]-1].potential = parameters->potential(_get_coord(xi, parameters->n[1]-1));
    }
    for (unsigned int yi = 0; yi < parameters->n[1]; yi++)
    {
        _points[0][yi].potential = parameters->potential(_get_coord(0, yi));
        _points[parameters->n[0]-1][yi].potential = parameters->potential(_get_coord(parameters->n[0]-1, yi));
    }
}