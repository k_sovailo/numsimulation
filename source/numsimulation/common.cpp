/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <numsimulation/common.h>
#include <stdexcept>
#include <fstream>
#include <iostream>
#include <ctype.h>

numsimulation::ParameterReader::ParameterReader(std::string filename)
{
    //Read file
    std::ifstream file(filename, std::ios::binary);
    if (!file.good()) throw std::runtime_error("numsimulation::ParameterReader::ParameterReader(): Error opening the file");
    file.seekg(0, std::ios::end);
    std::streampos size = file.tellg();
    file.seekg(0, std::ios::beg);
    std::string input(size, '\0');
    if (!file.read(&input[0], size)) throw std::runtime_error("numsimulation::ParameterReader::ParameterReader(): Error reading the file");

    //Parse
    //Parsing states:
    // 1. reading_value == false, name == "" (reading name, accepting spaces)
    // 2. reading_value == false, name != "" (reading name, rejecting spaces)
    // 3. reading_value == true, value == "" (reading name, accepting spaces)
    // 4. reading_value == true, value != "" (reading name, rejecting spaces)
    bool reading_value = false;
    std::string name, value;
    
    for (unsigned int i = 0; i <= size; i++)
    {
        //State 1
        if (!reading_value && name.empty())
        {
            if (i == size) break;
            else if (input[i] == ' ' || input[i] == '\t' || input[i] == '\n' || input[i] == '\r') {}
            else name.push_back(toupper(input[i]));
        }
        //State 2
        else if (!reading_value && !name.empty())
        {
            if (i == size || input[i] == '\n' || input[i] == '\r') throw std::runtime_error("numsimulation::ParameterReader::ParameterReader(): Unexpected character");
            else if (input[i] == ' ' || input[i] == '\t') reading_value = true;
            else name.push_back(toupper(input[i]));
        }
        //State 3
        else if (reading_value && value.empty())
        {
            if (i == size || input[i] == '\n' || input[i] == '\r') throw std::runtime_error("numsimulation::ParameterReader::ParameterReader(): Unexpected character");
            else if (input[i] == ' ' || input[i] == '\t') {}
            else value.push_back(toupper(input[i]));
        }
        //State 4
        else
        {
            if (i == size || input[i] == ' ' || input[i] == '\t' || input[i] == '\n' || input[i] == '\r')
            {
                std::pair<std::map<std::string, Parameter>::iterator, bool> r = _parameters.insert({name, {value, false}});
                reading_value = false;
                name.clear();
                value.clear();
                if (!r.second) throw std::runtime_error("numsimulation::ParameterReader::ParameterReader(): Parameter redefinition");
            }
            if (i == size) break;
            else if (input[i] == ' ' || input[i] == '\t' || input[i] == '\n' || input[i] == '\r') {}
            else value.push_back(toupper(input[i]));
        }
    }
}

std::string numsimulation::ParameterReader::get_string(std::string name)
{
    for (unsigned i = 0; i < name.size(); i++) name[i] = toupper(name[i]);
    std::map<std::string, Parameter>::iterator find = _parameters.find(name);
    if (find == _parameters.end()) throw std::runtime_error("numsimulation::ParameterReader::get_string(): Parameter does not exist");
    find->second.used = true;
    return find->second.value;
}

double numsimulation::ParameterReader::get_real(std::string name)
{
    for (unsigned i = 0; i < name.size(); i++) name[i] = toupper(name[i]);
    std::map<std::string, Parameter>::iterator find = _parameters.find(name);
    if (find == _parameters.end()) throw std::runtime_error("numsimulation::ParameterReader::get_real(): Parameter does not exist");
    find->second.used = true;
    char *end;
    double value = strtod(find->second.value.c_str(), &end);
    if (*end != '\0') std::runtime_error("numsimulation::ParameterReader::get_real(): Invalid real value");
    return value;
}

unsigned int numsimulation::ParameterReader::get_integer(std::string name)
{
    for (unsigned i = 0; i < name.size(); i++) name[i] = toupper(name[i]);
    std::map<std::string, Parameter>::iterator find = _parameters.find(name);
    if (find == _parameters.end()) throw std::runtime_error("numsimulation::ParameterReader::get_integer(): Parameter does not exist");
    find->second.used = true;
    char *end;
    unsigned int value = strtoul(find->second.value.c_str(), &end, 10);
    if (*end != '\0') std::runtime_error("numsimulation::ParameterReader::get_integer(): Invalid integer value");
    return value;
}

void numsimulation::ParameterReader::warn_unused()
{
    for (std::map<std::string, Parameter>::iterator parameter = _parameters.begin(); parameter != _parameters.end(); parameter++)
    {
        if (!parameter->second.used) std::cout << "Warning! Parameter " << parameter->first << " unused!\n";
    }
}

bool numsimulation::Position::operator<(const Position &b) const
{
    if (xi != b.xi) return xi < b.xi;
    else if (yi != b.yi) return yi < b.yi;
    else return (!upside_down && b.upside_down);
}

unsigned int numsimulation::get_shape(GridType typ)
{
    if (typ == GridType::triangular) return 3;
    else if (typ == GridType::hexagonal) return 6;
    else return 4;
}

Eigen::Vector2d numsimulation::get_center(Position position, GridType typ, Eigen::Vector2d origin, const double size[2])
{
    if (typ == GridType::triangular)
    {
        Eigen::Vector2d center = origin + Eigen::Vector2d(size[0] * (0.5 * position.yi + position.xi), size[1] * (0.5 * sqrt(3) * position.yi));
        if (position.upside_down) return center + Eigen::Vector2d(size[0] * (1.0 / 4.0), size[1] * (sqrt(3) / 12));
        else return center - Eigen::Vector2d(size[0] * (1.0 / 4.0), size[1] * (sqrt(3) / 12));
    }
    else if (typ == GridType::hexagonal)
    {
        return origin + Eigen::Vector2d(size[0] * (position.yi + 2.0 * position.xi), size[1] * (sqrt(3) * position.yi));
    }
    else
    {
        return origin + Eigen::Vector2d(size[0] * position.xi, size[1] * position.yi);
    }
}

std::vector<Eigen::Vector2d> numsimulation::get_points(Position position, GridType typ, Eigen::Vector2d origin, const double size[2])
{
    if (typ == GridType::triangular)
    {
        Eigen::Vector2d center = origin + Eigen::Vector2d(size[0] * (0.5 * position.yi + position.xi), size[1] * (0.5 * sqrt(3) * position.yi));
        std::vector<Eigen::Vector2d> result(3);
        if (position.upside_down)
        {
            result[0] = center + Eigen::Vector2d(size[0] * (3.0 / 4.0), size[1] * (sqrt(3) / 4));
            result[1] = center + Eigen::Vector2d(size[0] * (-1.0 / 4.0), size[1] * (sqrt(3) / 4));
            result[2] = center + Eigen::Vector2d(size[0] * (1.0 / 4.0), size[1] * (-sqrt(3) / 4));
        }
        else
        {
            result[0] = center + Eigen::Vector2d(size[0] * (-3.0 / 4.0), size[1] * (-sqrt(3) / 4));
            result[1] = center + Eigen::Vector2d(size[0] * (1.0 / 4.0), size[1] * (-sqrt(3) / 4));
            result[2] = center + Eigen::Vector2d(size[0] * (-1.0 / 4.0), size[1] * (sqrt(3) / 4));
        }
        return result;
    }
    else if (typ == GridType::hexagonal)
    {
        Eigen::Vector2d center = origin + Eigen::Vector2d(size[0] * (position.yi + 2.0 * position.xi), size[1] * (sqrt(3) * position.yi));
        std::vector<Eigen::Vector2d> result(6);
        result[0] = center + Eigen::Vector2d(0, size[1] * -1);
        result[1] = center + Eigen::Vector2d(size[0] * (sqrt(3) / 4), size[1] * (-1.0 / 2.0));
        result[2] = center + Eigen::Vector2d(size[0] * (sqrt(3) / 4), size[1] * (1.0 / 2.0));
        result[3] = center + Eigen::Vector2d(0, size[1] * 1);
        result[4] = center + Eigen::Vector2d(size[0] * (-sqrt(3) / 4), size[1] * (1.0 / 2.0));
        result[5] = center + Eigen::Vector2d(size[0] * (-sqrt(3) / 4), size[1] * (-1.0 / 2.0));
        return result;
    }
    else
    {
        Eigen::Vector2d center = origin + Eigen::Vector2d(size[0] * position.xi, size[1] * position.yi);
        std::vector<Eigen::Vector2d> result(4);
        result[0] = center + Eigen::Vector2d(size[0] * -0.5, size[1] * -0.5);
        result[1] = center + Eigen::Vector2d(size[0] * 0.5, size[1] * -0.5);
        result[2] = center + Eigen::Vector2d(size[0] * 0.5, size[1] * 0.5);
        result[3] = center + Eigen::Vector2d(size[0] * -0.5, size[1] * 0.5);
        return result;
    }
}

numsimulation::FaceNeighbor numsimulation::get_face_neighbor(Position position, GridType typ, unsigned int face)
{
    FaceNeighbor result{position, 0};
    if (typ == GridType::triangular)
    {
        int one = (position.upside_down ? -1 : 1);
        if (face == 0) { result.position.yi -= one; }
        else if (face == 2) { result.position.xi -= one; }
        result.position.upside_down = !position.upside_down;
        result.face = face;
    }
    else if (typ == GridType::hexagonal)
    {
        if (face == 0) { result.position.yi--; result.position.xi++; }
        else if (face == 1) { result.position.xi++; }
        else if (face == 2) { result.position.yi++; }
        else if (face == 3) { result.position.xi--; result.position.yi++; }
        else if (face == 4) { result.position.xi--; }
        else { result.position.yi--; }
        result.face = (face + 3) % 6;
    }
    else
    {
        if (face == 0) { result.position.yi--; }
        else if (face == 1) { result.position.xi++; }
        else if (face == 2) { result.position.yi++; }
        else { result.position.xi--; }
        result.face = (face + 2) % 4;
    }
    return result;
}

std::vector<numsimulation::PointNeighbor> numsimulation::get_point_neighbors(Position position, GridType typ, unsigned int point)
{
    if (typ == GridType::triangular)
    {
        std::vector<PointNeighbor> result(5, PointNeighbor{position, 0});
        int one = (position.upside_down ? -1 : 1);
        if (point == 0)
        {
            result[0].point = 2; result[0].position.xi -= one; result[0].position.upside_down = !position.upside_down;
            result[1].point = 1; result[1].position.xi -= one;
            result[2].point = 0; result[2].position.xi -= one; result[2].position.yi -= one; result[2].position.upside_down = !position.upside_down;
            result[3].point = 2; result[3].position.yi -= one;
            result[4].point = 1; result[4].position.yi -= one; result[4].position.upside_down = !position.upside_down;
        }
        else if (point == 1)
        {
            result[0].point = 0; result[0].position.yi -= one; result[0].position.upside_down = !position.upside_down;
            result[1].point = 2; result[1].position.xi += one; result[1].position.yi -= one;
            result[2].point = 1; result[2].position.xi += one; result[2].position.yi -= one; result[2].position.upside_down = !position.upside_down;
            result[3].point = 0; result[3].position.xi += one;
            result[4].point = 2; result[4].position.upside_down = !position.upside_down;
        }
        else
        {
            result[0].point = 1; result[0].position.upside_down = !position.upside_down;
            result[1].point = 0; result[1].position.yi += one;
            result[2].point = 2; result[2].position.xi -= one; result[2].position.yi += one; result[2].position.upside_down = !position.upside_down;
            result[3].point = 1; result[3].position.xi -= one; result[3].position.yi += one;
            result[4].point = 0; result[4].position.xi += one; result[4].position.upside_down = !position.upside_down;
        }
        return result;
    }
    else if (typ == GridType::hexagonal)
    {
        std::vector<PointNeighbor> result(2, PointNeighbor{position, 0});
        if (point == 0) { result[0].position.yi--; result[1].position.xi++; result[1].position.yi--; }
        else if (point == 1) { result[0].position.xi++; result[0].position.yi--; result[1].position.xi++; }
        else if (point == 2) { result[0].position.xi++; result[1].position.yi++; }
        else if (point == 3) { result[0].position.yi++; result[1].position.xi--; result[1].position.yi++; }
        else if (point == 4) { result[0].position.xi--; result[0].position.yi++; result[1].position.xi--; }
        else { result[0].position.xi--; result[1].position.yi--; }
        result[0].point = (point + 2) % 6;
        result[1].point = (point + 4) % 6;
        return result;
    }
    else
    {
        std::vector<PointNeighbor> result(3, PointNeighbor{position, 0});
        if (point == 0) { result[0].position.xi--; result[1].position.xi--; result[1].position.yi--; result[2].position.yi--; }
        else if (point == 1) { result[0].position.yi--; result[1].position.xi++; result[1].position.yi--; result[2].position.xi++; }
        else if (point == 2) { result[0].position.xi++; result[1].position.xi++; result[1].position.yi++; result[2].position.yi++; }
        else { result[0].position.yi++; result[1].position.xi--; result[1].position.yi++; result[2].position.xi--; }
        result[0].point = (point + 1) % 4;
        result[1].point = (point + 2) % 4;
        result[2].point = (point + 3) % 4;
        return result;
    }
}

numsimulation::Intersection::Intersection() : valid(false) {}

numsimulation::Intersection::Intersection(Eigen::Vector2d coord, Eigen::Vector2d vector, Eigen::Vector2d normal) : valid(true), coord(coord), vector(vector/vector.norm()), normal(normal/normal.norm()) {}

numsimulation::Circle::Circle(Eigen::Vector2d center, double radius, bool normal_inside) : _center(center), _radius(radius), _normal_inside(normal_inside) {}
numsimulation::Intersection numsimulation::Circle::intersection(Eigen::Vector2d a, Eigen::Vector2d b)
{
    double A = 1;
    double B = -2 * (_center - a).dot(b-a) / (b-a).norm();
    double C = (_center - a).squaredNorm() - sqr(_radius);
    double determinant = sqr(B) - 4 * A * C;
    if (determinant == 0)
    {
        double L = (-B) / (2 * A);
        if (L >= 0 && L <= (b-a).norm()) //Valid length
        {
            Eigen::Vector2d I = a + L * (b-a) / (b-a).norm();
            return Intersection(I, rotate_ccw(I - _center), _normal_inside ? (_center - I) : (I - _center));
        }
    }
    else if (determinant > 0)
    {
        double L1 = (-B - sqrt(determinant)) / (2 * A);
        if (L1 >= 0 && L1 <= (b-a).norm()) //Valid length
        {
            Eigen::Vector2d I = a + L1 * (b-a) / (b-a).norm();
            return Intersection(I, rotate_ccw(I - _center), _normal_inside ? (_center - I) : (I - _center));
        }
        double L2 = (-B + sqrt(determinant)) / (2 * A);
        if (L2 >= 0 && L2 <= (b-a).norm()) //Valid length
        {
            Eigen::Vector2d I = a + L2 * (b-a) / (b-a).norm();
            return Intersection(I, rotate_ccw(I - _center), _normal_inside ? (_center - I) : (I - _center));
        }
    }
    return Intersection();
}

numsimulation::Arc::Arc(Eigen::Vector2d center, double radius, double azimuth, double angle, bool normal_inside) : _center(center), _radius(radius), _azimuth(azimuth), _angle(angle), _normal_inside(normal_inside) {}
numsimulation::Intersection numsimulation::Arc::intersection(Eigen::Vector2d a, Eigen::Vector2d b)
{
    double A = 1;
    double B = -2 * (_center - a).dot(b - a) / (b - a).norm();
    double C = (_center - a).squaredNorm() - sqr(_radius);
    double determinant = sqr(B) - 4 * A * C;
    if (determinant == 0)
    {
        double L = (-B) / (2 * A);
        if (L >= 0 && L <= (b-a).norm()) //Valid length
        {
            Eigen::Vector2d I = a + L * (b-a) / (b-a).norm();
            double angle = atan2((I - _center).y(), (I - _center).x());
            if (angle >= _azimuth && angle <= (_azimuth + angle)) return Intersection(I, rotate_ccw(I - _center), _normal_inside ? (_center - I) : (I - _center)); //Valid azimuth
        }
    }
    else if (determinant > 0)
    {
        double L1 = (-B - sqrt(determinant)) / (2 * A);
        if (L1 >= 0 && L1 <= (b-a).norm()) //Valid length
        {
            Eigen::Vector2d I = a + L1 * (b-a) / (b-a).norm();
            double angle = atan2((I - _center).y(), (I - _center).x());
            if (angle >= _azimuth && angle <= (_azimuth + angle)) return Intersection(I, rotate_ccw(I - _center), _normal_inside ? (_center - I) : (I - _center)); //Valid azimuth
        }
        double L2 = (-B + sqrt(determinant)) / (2 * A);
        if (L2 >= 0 && L2 <= (b-a).norm()) //Valid length
        {
            Eigen::Vector2d I = a + L2 * (b-a) / (b-a).norm();
            double angle = atan2((I - _center).y(), (I - _center).x());
            if (angle >= _azimuth && angle <= (_azimuth + angle)) return Intersection(I, rotate_ccw(I - _center), _normal_inside ? (_center - I) : (I - _center)); //Valid azimuth
        }
    }
    return Intersection();
}

numsimulation::Line::Line(Eigen::Vector2d a, Eigen::Vector2d b, bool normal_cw) : _a(a), _b(b), _normal_cw(normal_cw) {}
numsimulation::Intersection numsimulation::Line::intersection(Eigen::Vector2d a, Eigen::Vector2d b)
{
    double t = ((a.x()-_a.x()) * (_a.y()-_b.y()) - (a.y()-_a.y()) * (_a.x()-_b.x())) / ((a.x()-b.x()) * (_a.y()-_b.y()) - (a.y()-b.y()) * (_a.x()-_b.x()));
    if (!(t >= 0 && t <= 1)) return Intersection();
    double u = ((a.x()-_a.x()) * (a.y()-b.y()) - (a.y()-_a.y()) * (a.x()-b.x())) / ((a.x()-b.x()) * (_a.y()-_b.y()) - (a.y()-b.y()) * (_a.x()-_b.x()));
    if (!(u >= 0 && u <= 1)) return Intersection();
    return Intersection(a * (1.0 - t) + b * t, _b - _a, _normal_cw ? rotate_cw(_b-_a) : rotate_ccw(_b-_a));
}

Eigen::Vector2d numsimulation::rotate_ccw(Eigen::Vector2d v) { return Eigen::Vector2d(v(1), -v(0)); }
Eigen::Vector2d numsimulation::rotate_cw(Eigen::Vector2d v) { return Eigen::Vector2d(-v(1), v(0)); }