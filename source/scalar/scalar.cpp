/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar/scalar.h>

/*
    Cells consist of points. Each point can be active, passive or unreached
    Unreached points have not yet been reached by the algorithm
    Active and passive points are both "reached" points, they are divided in two for optimization reasons
    Active points should be searched around, while the search around passive points is already complete and will bring no more results
    Active cell is a cell that has active points. The algorithm searches around active cells.
    Because the point is located in multiple cells simultaneously, the search is performed in each cell
    To prevent any inconsistency when searching along faces in different cells, the algorithm performs the search one time, and then saves the result
    
    Because adding/removing elements from set cannot be done while iterating through it, there are sets like "to_be_active", etc.
*/

namespace scalar
{
    enum class PointStatus
    {
        unreached,
        to_be_active,
        active,
        passive
    };

    struct TemporaryPoint
    {
        PointStatus status = PointStatus::unreached;

        Point *point = nullptr;
    };

    struct TemporaryFace
    {
        //Face probing system is somewhat redundant, but it's... acceptable...
        bool probed = false;
        bool valid = false;
        Eigen::Vector2d intersection;

        Point *point = nullptr;
        Face *face = nullptr;
    };

    struct TemporaryCell
    {
        std::vector<TemporaryPoint> points;
        std::vector<TemporaryFace> faces;
        double area;
        Eigen::Vector2d center;
        bool fixed_scalar = false;

        Cell *cell = nullptr;
        TemporaryCell(GridType grid_type);
    };
}

scalar::TemporaryCell::TemporaryCell(GridType grid_type)
{
    if (grid_type == GridType::triangular) { points.resize(3); faces.resize(3); }
    else if (grid_type == GridType::hexagonal) { points.resize(6); faces.resize(6); }
    else { points.resize(4); faces.resize(4); }
}


scalar::Point::Point(Eigen::Vector2d coord) : coord(coord) {}

scalar::Cell::Cell(bool fixed_scalar, double area, Eigen::Vector2d center) : fixed_scalar(fixed_scalar), area(area), center(center) {}

void scalar::Solver::_add_first_cell(std::map<Position, TemporaryCell> &active)
{
    TemporaryCell new_cell(_parameters->grid_type);
    new_cell.points[0].status = PointStatus::active;
    active.insert({Position(), new_cell});
    const std::vector<PointNeighbor> neighbors = get_point_neighbors(Position(), _parameters->grid_type, 0);
    for (unsigned int n = 0; n < neighbors.size(); n++)
    {
        new_cell = TemporaryCell(_parameters->grid_type);
        new_cell.points[neighbors[n].point].status = PointStatus::active;
        active.insert({neighbors[n].position, new_cell});
    }
}

void scalar::Solver::_add_all_cells(std::map<Position, TemporaryCell> &active, std::map<Position, TemporaryCell> &passive)
{
    while (!active.empty())
    {
        //Iterate through active, probe unprobed faces, add new cells to to_be_active
        std::map<Position, TemporaryCell> to_be_active;
        for (std::map<Position, TemporaryCell>::iterator cell = active.begin(); cell != active.end(); cell++)
        {
            //For each point
            std::vector<Eigen::Vector2d> points = get_points(cell->first, _parameters->grid_type, _parameters->grid_origin, _parameters->grid_size);
            for (unsigned int p = 0; p < get_shape(_parameters->grid_type); p++)
            {
                if (cell->second.points[p].status != PointStatus::active) continue;

                //Trying to probe counterclockwise
                const unsigned int next_ccw = ((p == (cell->second.points.size() - 1)) ? 0 : (p + 1));
                if (!cell->second.faces[p].probed)
                {
                    Intersection intersection;
                    bool fixed_scalar = false;
                    for (std::vector<Boundary>::const_iterator boundary = _parameters->boundaries.begin(); boundary != _parameters->boundaries.end(); boundary++)
                    {
                        Intersection new_intersection = boundary->figure->intersection(points[p], points[next_ccw]);
                        if (new_intersection.valid && (!intersection.valid || ((new_intersection.coord-points[p]).squaredNorm() < (intersection.coord-points[p]).squaredNorm())))
                        {
                            intersection = new_intersection;
                            if (boundary->fixed_scalar) fixed_scalar = true;
                        }
                    }
                    cell->second.faces[p].probed = true;
                    cell->second.faces[p].valid = intersection.valid;
                    cell->second.faces[p].intersection = intersection.coord;
                    const FaceNeighbor neighbor = get_face_neighbor(cell->first, _parameters->grid_type, p);
                    std::map<Position, TemporaryCell>::iterator find = active.find(neighbor.position);
                    find->second.faces[neighbor.face].probed = true;
                    find->second.faces[neighbor.face].valid = intersection.valid;
                    find->second.faces[neighbor.face].intersection = intersection.coord;
                    if (fixed_scalar)
                    {
                        cell->second.fixed_scalar = true;
                        find->second.fixed_scalar = true;
                    }
                }
                if (!cell->second.faces[p].valid && cell->second.points[next_ccw].status == PointStatus::unreached)
                {
                    cell->second.points[next_ccw].status = PointStatus::to_be_active;
                    const std::vector<PointNeighbor> neighbors = get_point_neighbors(cell->first, _parameters->grid_type, next_ccw);
                    for (std::vector<PointNeighbor>::const_iterator neighbor = neighbors.begin(); neighbor != neighbors.end(); neighbor++)
                    {
                        if (to_be_active.find(neighbor->position) != to_be_active.end()) //to_be_active points remain to_be_active
                        {
                            to_be_active.find(neighbor->position)->second.points[neighbor->point].status = PointStatus::to_be_active;
                        }
                        else if (active.find(neighbor->position) != active.end()) //Active points remain active
                        {
                            active.find(neighbor->position)->second.points[neighbor->point].status = PointStatus::to_be_active;
                        }
                        else if (passive.find(neighbor->position) != passive.end()) //Passive points become to_be_active
                        {
                            passive.find(neighbor->position)->second.points[neighbor->point].status = PointStatus::to_be_active;
                            to_be_active.insert(*passive.find(neighbor->position));
                            passive.erase(neighbor->position);
                        }
                        else //Create to_be_active point
                        {
                            TemporaryCell new_cell(_parameters->grid_type);
                            new_cell.points[neighbor->point].status = PointStatus::to_be_active;
                            to_be_active.insert({neighbor->position, new_cell});
                        }
                    }
                }

                //Trying to probe clockwise
                unsigned int next_cw = ((p == 0) ? (cell->second.points.size() - 1) : (p - 1));
                if (!cell->second.faces[next_cw].probed)
                {
                    Intersection intersection;
                    bool fixed_scalar = false;
                    for (std::vector<Boundary>::const_iterator boundary = _parameters->boundaries.begin(); boundary != _parameters->boundaries.end(); boundary++)
                    {
                        Intersection new_intersection = boundary->figure->intersection(points[p], points[next_cw]);
                        if (new_intersection.valid && (!intersection.valid || ((new_intersection.coord-points[p]).squaredNorm() < (intersection.coord-points[p]).squaredNorm())))
                        {
                            intersection = new_intersection;
                            if (boundary->fixed_scalar) fixed_scalar = true;
                        }
                    }
                    cell->second.faces[next_cw].probed = true;
                    cell->second.faces[next_cw].valid = intersection.valid;
                    cell->second.faces[next_cw].intersection = intersection.coord;
                    const FaceNeighbor neighbor = get_face_neighbor(cell->first, _parameters->grid_type, next_cw);
                    std::map<Position, TemporaryCell>::iterator find = active.find(neighbor.position);
                    find->second.faces[neighbor.face].probed = true;
                    find->second.faces[neighbor.face].valid = intersection.valid;
                    find->second.faces[neighbor.face].intersection = intersection.coord;
                    if (fixed_scalar)
                    {
                        cell->second.fixed_scalar = true;
                        find->second.fixed_scalar = true;
                    }
                }
                if (!cell->second.faces[next_cw].valid && cell->second.points[next_cw].status == PointStatus::unreached)
                {
                    cell->second.points[next_cw].status = PointStatus::to_be_active;
                    const std::vector<PointNeighbor> neighbors = get_point_neighbors(cell->first, _parameters->grid_type, next_cw);
                    for (std::vector<PointNeighbor>::const_iterator neighbor = neighbors.begin(); neighbor != neighbors.end(); neighbor++)
                    {
                        if (to_be_active.find(neighbor->position) != to_be_active.end()) //to_be_active points remain to_be_active
                        {
                            to_be_active.find(neighbor->position)->second.points[neighbor->point].status = PointStatus::to_be_active;
                        }
                        else if (active.find(neighbor->position) != active.end()) //Active points remain active
                        {
                            active.find(neighbor->position)->second.points[neighbor->point].status = PointStatus::to_be_active;
                        }
                        else if (passive.find(neighbor->position) != passive.end()) //Passive points become to_be_active
                        {
                            passive.find(neighbor->position)->second.points[neighbor->point].status = PointStatus::to_be_active;
                            to_be_active.insert(*passive.find(neighbor->position));
                            passive.erase(neighbor->position);
                        }
                        else //Create to_be_active point
                        {
                            TemporaryCell new_cell(_parameters->grid_type);
                            new_cell.points[neighbor->point].status = PointStatus::to_be_active;
                            to_be_active.insert({neighbor->position, new_cell});
                        }
                    }
                }
            }
        }

        //Add fresh active cells
        active.insert(to_be_active.begin(), to_be_active.end());

        //Make to_be_active points active, make active points passive, divide active into new_active and passive
        std::map<Position, TemporaryCell> new_active;
        for (std::map<Position, TemporaryCell>::iterator cell = active.begin(); cell != active.end(); cell++)
        {
            bool active = false;
            for (unsigned int p = 0; p < cell->second.points.size(); p++)
            {
                if (cell->second.points[p].status == PointStatus::to_be_active)
                {
                    cell->second.points[p].status = PointStatus::active;
                    active = true;
                }
                else if (cell->second.points[p].status == PointStatus::active)
                {
                    cell->second.points[p].status = PointStatus::passive;
                }
            }
            if (active) new_active.insert(*cell);
            else passive.insert(*cell);
        }
        active = new_active;
    }
}

void scalar::Solver::_calculate_area(std::map<Position, TemporaryCell> &cells)
{
    for (std::map<Position, TemporaryCell>::iterator i = cells.begin(); i != cells.end(); i++)
    {
        //Create list of points
        std::vector<Eigen::Vector2d> points = get_points(i->first, _parameters->grid_type, _parameters->grid_origin, _parameters->grid_size);
        std::vector<Eigen::Vector2d> point_list;
        for (unsigned int p = 0; p < get_shape(_parameters->grid_type); p++)
        {
            unsigned int next_ccw = ((p == (i->second.points.size() - 1)) ? 0 : (p + 1));
            if (i->second.points[p].status == PointStatus::passive) point_list.push_back(points[p]);
            if (i->second.points[p].status != i->second.points[next_ccw].status) point_list.push_back(i->second.faces[p].intersection);
        }

        //Calculate area
        i->second.area = 0;
        i->second.center = Eigen::Vector2d::Zero();
        for (unsigned int p = 1; p < (point_list.size() - 1); p++)
        {
            double a = (point_list[p] - point_list[0]).norm();
            double b = (point_list[p+1] - point_list[0]).norm();
            double c = (point_list[p+1] - point_list[p]).norm();
            double s = 0.5 * (a + b + c);
            double local_area = sqrt(s * (s - a) * (s - b) * (s - c));
            Eigen::Vector2d local_center = (point_list[0] + point_list[p] + point_list[p+1]) / 3;
            i->second.area += local_area;
            i->second.center += (local_center * local_area);
        }
        i->second.center = i->second.center / i->second.area;
    }
}

void scalar::Solver::_apply_failed_cells(std::map<Position, TemporaryCell> &cells)
{
    for (std::map<Position, TemporaryCell>::iterator cell = cells.begin(); cell != cells.end(); cell++)
    {
        //Applying conditions of failed cells to neighboring cells
        if (cell->second.area < _parameters->area_threshold)
        {
            for (unsigned int f = 0; f < get_shape(_parameters->grid_type); f++)
            {
                const FaceNeighbor neighbor = get_face_neighbor(cell->first, _parameters->grid_type, f);
                std::map<Position, TemporaryCell>::iterator find = cells.find(neighbor.position);
                if (find != cells.end() && cell->second.area >= _parameters->area_threshold) find->second.fixed_scalar = cell->second.fixed_scalar;
            }
        }
    }
}

void scalar::Solver::_create_cells(std::map<Position, TemporaryCell> &cells)
{
    //Creating cells
    for (std::map<Position, TemporaryCell>::iterator cell = cells.begin(); cell != cells.end(); cell++)
    {
        if (cell->second.area >= _parameters->area_threshold)
        {
            _cells.insert(cell->second.cell = new Cell(cell->second.fixed_scalar, cell->second.area, cell->second.center));
        }
    }
}

void scalar::Solver::_create_points(std::map<Position, TemporaryCell> &cells)
{
    //Creating points, faces and adding neighbors
    for (std::map<Position, TemporaryCell>::iterator cell = cells.begin(); cell != cells.end(); cell++)
    {
        if (cell->second.cell == nullptr) continue;

        //Creating points
        std::vector<Eigen::Vector2d> points = get_points(cell->first, _parameters->grid_type, _parameters->grid_origin, _parameters->grid_size);
        for (unsigned int p = 0; p < cell->second.points.size(); p++)
        {
            //Regular points
            if (cell->second.points[p].status == PointStatus::passive)
            {
                if (cell->second.points[p].point == nullptr)
                {
                    _points.insert(cell->second.points[p].point = new Point(points[p]));
                    const std::vector<PointNeighbor> neighbors = get_point_neighbors(cell->first, _parameters->grid_type, p);
                    for (std::vector<PointNeighbor>::const_iterator neighbor = neighbors.begin(); neighbor != neighbors.end(); neighbor++)
                        cells.find(neighbor->position)->second.points[neighbor->point].point = cell->second.points[p].point;
                }
                cell->second.cell->points.push_back(cell->second.points[p].point);
            }

            //Points on faces
            unsigned int next_ccw = ((p == (cell->second.points.size() - 1)) ? 0 : (p + 1));
            if (cell->second.points[p].status != cell->second.points[next_ccw].status)
            {
                if (cell->second.faces[p].point == nullptr)
                {
                    _points.insert(cell->second.faces[p].point = new Point(cell->second.faces[p].intersection));
                    const FaceNeighbor neighbor = get_face_neighbor(cell->first, _parameters->grid_type, p);
                    cells.find(neighbor.position)->second.faces[neighbor.face].point = cell->second.faces[p].point;
                }
                cell->second.cell->points.push_back(cell->second.faces[p].point);
            }
        }
    }
}

void scalar::Solver::_create_faces(std::map<Position, TemporaryCell> &cells)
{
    //Creating points, faces and adding neighbors
    for (std::map<Position, TemporaryCell>::iterator cell = cells.begin(); cell != cells.end(); cell++)
    {
        if (cell->second.cell == nullptr) continue;

        //Creating faces
        Face *irregular = nullptr;
        for (unsigned int p = 0; p < cell->second.faces.size(); p++)
        {
            unsigned int next_ccw = ((p == (cell->second.points.size() - 1)) ? 0 : (p + 1));
            if (cell->second.points[p].status == PointStatus::passive || cell->second.points[next_ccw].status == PointStatus::passive)
            {
                const FaceNeighbor neighbor = get_face_neighbor(cell->first, _parameters->grid_type, p);
                if (cell->second.faces[p].face == nullptr)
                {
                    _faces.insert(cell->second.faces[p].face = new Face());
                    cells.find(neighbor.position)->second.faces[neighbor.face].face = cell->second.faces[p].face;
                }
                cell->second.cell->faces.push_back(cell->second.faces[p].face);
                cell->second.cell->neighbors.push_back(cells.find(neighbor.position)->second.cell);
            }
            if (cell->second.points[p].status == PointStatus::passive && cell->second.points[next_ccw].status == PointStatus::passive)
            {
                cell->second.faces[p].face->point[0] = cell->second.points[p].point;
                cell->second.faces[p].face->point[1] = cell->second.points[next_ccw].point;
            }
            if (cell->second.points[p].status == PointStatus::passive && cell->second.points[next_ccw].status != PointStatus::passive)
            {
                cell->second.faces[p].face->point[0] = cell->second.points[p].point;
                cell->second.faces[p].face->point[1] = cell->second.faces[p].point;
                //Opening irregular face
                _faces.insert(irregular = new Face);
                cell->second.cell->faces.push_back(irregular);
                cell->second.cell->neighbors.push_back(nullptr);
                irregular->point[0] = cell->second.faces[p].point;
            }
            if (cell->second.points[p].status != PointStatus::passive && cell->second.points[next_ccw].status == PointStatus::passive)
            {
                cell->second.faces[p].face->point[0] = cell->second.faces[p].point;
                cell->second.faces[p].face->point[1] = cell->second.points[next_ccw].point;
                //Closing irregular face
                if (irregular != nullptr)
                {
                    irregular->point[1] = cell->second.faces[p].point;
                    irregular = nullptr;
                }
            }
        }
        if (irregular != nullptr)
        {
            //Closing irregular face
            for (unsigned int p = 0; p < cell->second.faces.size(); p++)
            {
                if (cell->second.faces[p].point != nullptr) { irregular->point[1] = cell->second.faces[p].point; break; }
            }
        }
    }
}

void scalar::Solver::_precalculate_faces()
{
    for (std::set<Face*>::iterator iface = _faces.begin(); iface != _faces.end(); iface++)
    {
        Face *face = *iface;
        Eigen::Vector2d face_vector = face->point[0]->coord - face->point[1]->coord;
        face->normal = rotate_cw(face_vector) / face_vector.norm();
        face->length = face_vector.norm();
        face->center = (face->point[0]->coord + face->point[1]->coord) / 2;
    }
}

void scalar::Solver::_precalculate_cells_faces()
{
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        cell->face_constants.resize(cell->faces.size());
        for (unsigned int f = 0; f < cell->faces.size(); f++)
        {
            if (cell->neighbors[f] == nullptr) continue; //No cell on other side

            Eigen::Vector2d face_normal = cell->faces[f]->normal;
            cell->face_constants[f].face_normal_sign = 1;
            if ((cell->center - cell->faces[f]->center).dot(face_normal) < 0.0) { face_normal *= -1; cell->face_constants[f].face_normal_sign *= -1; }
            Eigen::Vector2d face_vector = cell->faces[f]->point[0]->coord - cell->faces[f]->point[1]->coord;
            cell->face_constants[f].face_cell_distance = (cell->center - cell->faces[f]->center).dot(face_normal);
            cell->face_constants[f].cell_cell_distance = (cell->center - cell->neighbors[f]->center).dot(face_normal);
        }
    }
}

void scalar::Solver::_setup_scalar()
{
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        cell->scalar = _parameters->scalar(cell->center, 0);
    }
}

scalar::Solver::Solver(const AbstractParameters *parameters, std::string filename) : _parameters(parameters), _filename(filename)
{
    //Define sets, add first active point
    std::map<Position, TemporaryCell> passive, active;
    _add_first_cell(active);
    _add_all_cells(active, passive);
    _calculate_area(passive);
    _apply_failed_cells(passive);
    _create_cells(passive);
    _create_points(passive);
    _create_faces(passive);
    _precalculate_faces();
    _precalculate_cells_faces();
    _setup_scalar();

    //Opening file
    _file = std::ofstream(_filename, std::ios::binary);
    if (!_file.good()) throw std::runtime_error("scalar::Solver::_output(): Cannot open file");
    
    //Write header
    double min[2] = { std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity() };
    double max[2] = { -std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity() };
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        if (cell->center(0) < min[0]) min[0] = cell->center(0);
        if (cell->center(1) < min[1]) min[1] = cell->center(1);
        if (cell->center(0) > max[0]) max[0] = cell->center(0);
        if (cell->center(1) > max[1]) max[1] = cell->center(1);
    }
    _file << "NX " << (unsigned int)((max[0] - min[0]) / _parameters->grid_size[0]) << " NY " << (unsigned int)((max[1] - min[1]) / _parameters->grid_size[1]) << '\n';
}

scalar::Solver::~Solver()
{
    for (std::set<Point*>::iterator i = _points.begin(); i != _points.end(); i++)
    {
        delete (*i);
    }

    for (std::set<Face*>::iterator i = _faces.begin(); i != _faces.end(); i++)
    {
        delete (*i);
    }
    
    for (std::set<Cell*>::iterator i = _cells.begin(); i != _cells.end(); i++)
    {
        delete (*i);
    }
}