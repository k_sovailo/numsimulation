/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar/scalar.h>
#include <iostream>

void scalar::Solver::_setup_fixed_scalar()
{
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        if (cell->fixed_scalar) cell->scalar = _parameters->scalar(cell->center, _time);
    }
}

void scalar::Solver::step()
{
    _setup_fixed_scalar();

    //Processing
    for (std::set<Cell*>::iterator i = _cells.begin(); i != _cells.end(); i++)
    {
        Cell *cell = *i;
        if (cell->fixed_scalar) continue; //Skip fixed_scalar cells
        
        double incoming_flow = 0;
        for (unsigned int f = 0; f < cell->faces.size(); f++)
        {
            //Precalculating
            if (cell->neighbors[f] == nullptr) continue; //No cell on other side, no flow
            const Eigen::Vector2d face_normal = cell->face_constants[f].face_normal_sign * cell->faces[f]->normal;
            const double face_length = cell->faces[f]->length;
            const double face_cell_distance = cell->face_constants[f].face_cell_distance;
            const double cell_cell_distance = cell->face_constants[f].cell_cell_distance;
            const double face_velocity = (_parameters->velocity(cell->faces[f]->center, _time)).dot(face_normal); //incoming flow

            //Calculating face scalar
            double face_scalar;
            if (_parameters->method == Method::linear)
            {
                face_scalar = (cell->scalar * (cell_cell_distance - face_cell_distance) + cell->neighbors[f]->scalar * face_cell_distance) / cell_cell_distance;
            }
            else if (_parameters->method == Method::upwind)
            {
                face_scalar = (face_velocity > 0.0) ? cell->neighbors[f]->scalar : cell->scalar;
            }
            else
            {
                const double pe = _parameters->density * face_velocity * cell_cell_distance / _parameters->diffusion;
                face_scalar = cell->scalar + (cell->neighbors[f]->scalar - cell->scalar) * (exp(face_cell_distance / cell_cell_distance * pe) - 1) / (exp(pe) - 1);
            }

            //Calculating flow
            incoming_flow += _parameters->density * face_velocity * face_scalar * face_length; //Convective
            incoming_flow += _parameters->diffusion * (cell->neighbors[f]->scalar - cell->scalar) * face_length / cell_cell_distance; //Diffusive
        }
        cell->new_scalar = cell->scalar + _parameters->step * incoming_flow / (_parameters->density * cell->area);
    }

    //Postprocessing
    for (std::set<Cell*>::iterator i = _cells.begin(); i != _cells.end(); i++)
    {
        if (!(*i)->fixed_scalar) (*i)->scalar = (*i)->new_scalar;
    }

    //Progressing time
    std::streamsize precision = std::cout.precision(4);
    std::cout << "Time step " << _time << " --> " << _time + _parameters->step << " done\n";
    std::cout.precision(precision);
    _time += _parameters->step;
}