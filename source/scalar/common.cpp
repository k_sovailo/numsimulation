/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar/scalar.h>

double scalar::Solver::time() const
{
    return _time;
}