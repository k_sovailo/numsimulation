/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar/scalar.h>

void scalar::Solver::output()
{
    _file << "T " << _time << "\n";

    //Write data
    for (std::set<Cell*>::iterator icell = _cells.begin(); icell != _cells.end(); icell++)
    {
        Cell *cell = *icell;
        _file << "X " << cell->center(0) << " Y " << cell->center(1) << " S " << cell->scalar;
        if (_parameters->exact) _file << " ES " << _parameters->scalar(cell->center, _time);
        _file << "\n";
    }
}