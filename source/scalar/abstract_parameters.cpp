/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar/scalar.h>

scalar::AbstractParameters::AbstractParameters(numsimulation::ParameterReader &reader)
{
    //Grid parameters
    if (reader.get_string("GRID") == "TRIANGULAR") grid_type = GridType::triangular;
    else if (reader.get_string("GRID") == "SQUARE") grid_type = GridType::square;
    else if (reader.get_string("GRID") == "HEXAGONAL") grid_type = GridType::hexagonal;
    else throw std::runtime_error("potential::AbstractParameters::AbstractParameters(): Unrecognized grid type");
    grid_size[0] = reader.get_real("GRID_SIZE_X"); grid_size[1] = reader.get_real("GRID_SIZE_Y");
    area_threshold = reader.get_real("AREA_THRESHOLD");

    // Solver parameters
    if (reader.get_string("METHOD") == "LINEAR") method = Method::linear;
    else if (reader.get_string("METHOD") == "UPWIND") method = Method::upwind;
    else if (reader.get_string("METHOD") == "EXPONENTIAL") method = Method::exponential;
    else throw std::runtime_error("_main(): Unrecognized numerical method");
    density = reader.get_real("DENSITY");
    diffusion = reader.get_real("DIFFUSION");
    step = reader.get_real("STEP");

    exact = false;
}