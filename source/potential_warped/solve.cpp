/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_warped/potential_warped.h>
#include <numsimulation/common.h>
#include <iostream>
using namespace numsimulation;

void potential_warped::Solver::solve()
{
    //Solve
    const double dxi = (_parameters->max[0] - _parameters->min[0]) / (_parameters->n[0] - 1);
    const double deta = (_parameters->max[1] - _parameters->min[1]) / (_parameters->n[1] - 1);
    const double dxi2 = sqr(dxi);
    const double deta2 = sqr(deta);
    double previous_residual = std::numeric_limits<double>::infinity();
    unsigned int fail_count = 0;
    unsigned int iteration = 0;
    while (true)
    {
        iteration++;
        double max_residual = -std::numeric_limits<double>::infinity();

        //Main calculation
        for (unsigned int xii = 1; xii < _parameters->n[0] - 1; xii++)
        {
            // Eta 0, wall
            double multiplied_without_center = (-0.5*_points[xii][2].potential + 2.0*_points[xii][1].potential) / deta;
            double center_coefficient = -1.5 / deta;
            double multiplied_only_center = center_coefficient * _points[xii][0].potential;
            //Residual A*x-b
            double residual = multiplied_without_center + multiplied_only_center;
            if (abs(residual) > max_residual) max_residual = abs(residual);
            //New potential (Gauss-Seidel or Jacobi algorithm)
            double new_potential = -multiplied_without_center / center_coefficient;
            new_potential = _parameters->beta * new_potential + (1.0 - _parameters->beta) * _points[xii][0].potential;
            (_parameters->gauss ? _points[xii][0].potential : _points[xii][0].new_potential) = new_potential;

            // Central points
            for (unsigned int etai = 1; etai < _parameters->n[1] - 1; etai++)
            {
                multiplied_without_center = (
                    _points[xii][etai].a_dp_dxi2 * (_points[xii-1][etai].potential + _points[xii+1][etai].potential) / dxi2 +
                    _points[xii][etai].a_dp_deta2 * (_points[xii][etai-1].potential + _points[xii][etai+1].potential) / deta2 +
                    _points[xii][etai].a_dp_dxieta * 0.25 * (_points[xii-1][etai-1].potential + _points[xii+1][etai+1].potential - _points[xii-1][etai+1].potential - _points[xii+1][etai-1].potential) / dxi / deta +
                    _points[xii][etai].a_dp_dxi * 0.5 * (_points[xii+1][etai].potential - _points[xii-1][etai].potential) / dxi +
                    _points[xii][etai].dp_deta * 0.5 * (_points[xii][etai+1].potential - _points[xii][etai-1].potential) / deta
                );
                center_coefficient = (
                    -2 * _points[xii][etai].a_dp_dxi2 / dxi2 +
                    -2 * _points[xii][etai].a_dp_deta2 / deta2
                );
                multiplied_only_center = center_coefficient * _points[xii][etai].potential;
                //Residual A*x-b
                residual = multiplied_without_center + multiplied_only_center;
                if (abs(residual) > max_residual) max_residual = abs(residual);
                //New potential (Gauss-Seidel or Jacobi algorithm)
                new_potential = -multiplied_without_center / center_coefficient;
                new_potential = _parameters->beta * new_potential + (1.0 - _parameters->beta) * _points[xii][etai].potential;
                (_parameters->gauss ? _points[xii][etai].potential : _points[xii][etai].new_potential) = new_potential;
            }

            // Eta _parameters->n[1]-1, wall
            multiplied_without_center = (-0.5*_points[xii][_parameters->n[1]-3].potential + 2.0*_points[xii][_parameters->n[1]-2].potential) / deta;
            center_coefficient = -1.5 / deta;
            multiplied_only_center = center_coefficient * _points[xii][_parameters->n[1]-1].potential;
            //Residual A*x-b
            residual = multiplied_without_center + multiplied_only_center;
            if (abs(residual) > max_residual) max_residual = abs(residual);
            //New potential (Gauss-Seidel or Jacobi algorithm)
            new_potential = -multiplied_without_center / center_coefficient;
            new_potential = _parameters->beta * new_potential + (1.0 - _parameters->beta) * _points[xii][_parameters->n[1]-1].potential;
            (_parameters->gauss ? _points[xii][_parameters->n[1]-1].potential : _points[xii][_parameters->n[1]-1].new_potential) = new_potential;
        }

        //Applying new_potential (Jacobi algorithm)
        if (!_parameters->gauss)
        {
            for (unsigned int xii = 1; xii < _parameters->n[0] - 1; xii++)
            {
                for (unsigned int etai = 0; etai < _parameters->n[1]; etai++)
                {
                    _points[xii][etai].potential = _points[xii][etai].new_potential;
                }
            }
        }

        //Logics
        if (max_residual < _parameters->max_residual)
        {
            std::cout << "Done in " << iteration << " iterations\n";
            break;
        }
        if (iteration > _parameters->max_iteration)
        {
            throw std::runtime_error("Number of iterations exceeded");
        }
        if (max_residual > previous_residual)
        {
            if (fail_count++ > 100) throw std::runtime_error("Residual does not converge");
        }
        previous_residual = max_residual;
    }
}