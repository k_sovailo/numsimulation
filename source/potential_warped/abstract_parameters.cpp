/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_warped/potential_warped.h>

potential_warped::AbstractParameters::AbstractParameters(numsimulation::ParameterReader &reader)
{
    // Geometry parameters
    min[0] = reader.get_real("XIMIN"); min[1] = reader.get_real("ETAMIN");
    max[0] = reader.get_real("XIMAX"); max[1] = reader.get_real("ETAMAX");

    // Grid parameters
    n[0] = reader.get_integer("NXI"); n[1] = reader.get_integer("NETA");
        
    // Solver parameters
    max_iteration = reader.get_integer("MAX_ITERATION");
    max_residual = reader.get_real("MAX_RESIDUAL");
    beta = reader.get_real("BETA");
    if (reader.get_string("SOLVER") == "GAUSS") gauss = true;
    else if (reader.get_string("SOLVER") == "JACOBI") gauss = false;
    else throw std::runtime_error("potential_warped::AbstractParameters::AbstractParameters(): unrecognized solver");

    exact = false;
}