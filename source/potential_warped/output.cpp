/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_warped/potential_warped.h>
#include <fstream>

void potential_warped::Solver::output()
{
    //Open file
    std::ofstream file(_filename, std::ios::binary);
    if (!file.good()) throw std::runtime_error("potential_warped::Solver::_output(): Cannot open file");
    
    //Write header
    file << "NX " << _parameters->n[0] << " NY " << _parameters->n[1] << '\n';
    file << "T 0\n";

    //Write data
    for (unsigned int etai = 0; etai < _parameters->n[1]; etai++)
    {
        for (unsigned int xii = 0; xii < _parameters->n[0]; xii++)
        {
            Eigen::Vector2d xy = _parameters->get_xy(_get_xieta(xii, etai));
            file << "X " << xy.x() << " Y " << xy.y() << " H " << _points[xii][etai].potential;
            if (_parameters->exact) file << " EH " << _parameters->potential(xy);
            file << "\n";
        }
    }
}
