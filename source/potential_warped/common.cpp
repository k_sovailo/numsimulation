/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_warped/potential_warped.h>

Eigen::Vector2d potential_warped::Solver::_get_xieta(unsigned int xii, unsigned int etai) const
{
    return Eigen::Vector2d(
        (_parameters->min[0] * (_parameters->n[0] - xii - 1) + _parameters->max[0] * xii) / (_parameters->n[0] - 1),
        (_parameters->min[1] * (_parameters->n[1] - etai - 1) + _parameters->max[1] * etai) / (_parameters->n[1] - 1)
    );
}