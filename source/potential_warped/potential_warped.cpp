/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_warped/potential_warped.h>
#include <numsimulation/common.h>
using namespace numsimulation;

potential_warped::Solver::Solver(const AbstractParameters *parameters, std::string filename) : _parameters(parameters), _filename(filename)
{
    //Allocate
    _points.resize(parameters->n[0]);
    for (unsigned int xii = 0; xii < parameters->n[0]; xii++) _points[xii].resize(parameters->n[1]);

    //Setup
    for (unsigned int etai = 0; etai < parameters->n[1]; etai++)
    {
        _points[0][etai].potential = parameters->potential(parameters->get_xy(_get_xieta(0, etai)));
        _points[parameters->n[0]-1][etai].potential = parameters->potential(parameters->get_xy(_get_xieta(parameters->n[0]-1, etai)));
    }

    // Precalculate coefficients
    for (unsigned int xii = 0; xii < parameters->n[0]; xii++)
    {
        for (unsigned int etai = 0; etai < parameters->n[1]; etai++)
        {
            Eigen::Vector2d xieta = _get_xieta(xii, etai);
            _points[xii][etai].a_dp_dxi2 = sqr(parameters->get_dxi_dx(xieta)) + sqr(parameters->get_dxi_dy(xieta));
            _points[xii][etai].a_dp_deta2 = sqr(parameters->get_deta_dx(xieta)) + sqr(parameters->get_deta_dy(xieta));
            _points[xii][etai].a_dp_dxieta = 2 * (parameters->get_dxi_dx(xieta) * parameters->get_deta_dx(xieta) + parameters->get_dxi_dy(xieta) * parameters->get_deta_dy(xieta));
            _points[xii][etai].a_dp_dxi = parameters->get_dxi_dx2(xieta) + parameters->get_dxi_dy2(xieta);
            _points[xii][etai].dp_deta = parameters->get_deta_dx2(xieta) + parameters->get_deta_dy2(xieta);
        }
    }
}