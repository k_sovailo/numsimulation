#!/usr/bin/python3
import sys
import numpy as np
import matplotlib.pyplot as plt

class Boundary:
    """Boundary of array"""
    def __init__(self, array = None):
        """Creates boundary"""
        if array is None:
            self.xmin = np.Infinity
            self.xmax = -np.Infinity
            self.ymin = np.Infinity
            self.ymax = -np.Infinity
        else:
            self.xmin = np.min(array[:,0])
            self.xmax = np.max(array[:,0])
            self.ymin = np.min(array[:,1])
            self.ymax = np.max(array[:,1])

    def merge(self, boundary):
        """Merges another boundary"""
        self.xmin = boundary.xmin = np.min((self.xmin, boundary.xmin))
        self.xmax = boundary.xmax = np.max((self.xmax, boundary.xmax))
        self.ymin = boundary.ymin = np.min((self.ymin, boundary.ymin))
        self.ymax = boundary.ymax = np.max((self.ymax, boundary.ymax))

class Entry:
    """Data frame: point array, matrix and boundary"""
    def __init__(self):
        """Creates data frame"""
        self.array = np.empty((0,3))
        self.matrix = None

    def valid(self):
        """Returns whether the entry is valid"""
        return len(self.array) != 0
    
    def empty(self):
        """Returns whether the entry is empty"""
        return len(self.array) == 0
    
    def add_point(self, x, y, scalar):
        """Adds data point"""
        self.array = np.vstack((self.array, (x, y, scalar)))

    def generate_matrix(self, boundary, nx, ny):
        """Creates matrix from boundary and data array"""
        self.matrix = np.zeros((nx, ny))
        count = np.zeros((nx, ny))
        for scalar in self.array:
            xi = (nx - 1) * (scalar[0] - boundary.xmin) / (boundary.xmax - boundary.xmin)
            yi = (ny - 1) * (scalar[1] - boundary.ymin) / (boundary.ymax - boundary.ymin)
            if np.round(xi) == xi and np.round(yi) == yi:
                self.matrix [int(xi),int(yi)] += scalar[2]
                count       [int(xi),int(yi)] += 1
            elif np.round(xi) == xi and np.round(yi) != yi:
                yf = yi - np.floor(yi)
                yc = np.ceil(yi) - yi
                self.matrix [int(xi),int(np.floor(yi))] += scalar[2] * yc
                count       [int(xi),int(np.floor(yi))] += yc
                self.matrix [int(xi),int(np.ceil(yi))]  += scalar[2] * yf
                count       [int(xi),int(np.ceil(yi))]  += yf
            elif np.round(xi) != xi and np.round(yi) == yi:
                xf = xi - np.floor(xi)
                xc = np.ceil(xi) - xi
                self.matrix [int(np.floor(xi)),int(yi)] += scalar[2] * xc
                count       [int(np.floor(xi)),int(yi)] += xc
                self.matrix [int(np.ceil(xi)),int(yi)]  += scalar[2] * xf
                count       [int(np.ceil(xi)),int(yi)]  += xf
            else:
                xf = xi - np.floor(xi)
                xc = np.ceil(xi) - xi
                yf = yi - np.floor(yi)
                yc = np.ceil(yi) - yi
                self.matrix [int(np.floor(xi)),int(np.floor(yi))] += scalar[2] * xc * yc
                count       [int(np.floor(xi)),int(np.floor(yi))] += xc * yc
                self.matrix [int(np.floor(xi)),int(np.ceil(yi))]  += scalar[2] * xc * yf
                count       [int(np.floor(xi)),int(np.ceil(yi))]  += xc * yf
                self.matrix [int(np.ceil(xi)), int(np.floor(yi))] += scalar[2] * xf * yc
                count       [int(np.ceil(xi)), int(np.floor(yi))] += xf * yc
                self.matrix [int(np.ceil(xi)), int(np.ceil(yi))]  += scalar[2] * xf * yf
                count       [int(np.ceil(xi)), int(np.ceil(yi))]  += xf * yf
        nan = np.empty((nx, ny))
        nan.fill(np.NaN)
        self.matrix =  np.divide(self.matrix, count, where=count!=0, out=nan)

class Frame:
    """Data point for specific time, may include multiple frames"""
    def __init__(self, time):
        """Data point"""
        self.time = time
        self.boundary = None

        self.potential = Entry()
        self.potential_exact = Entry()

        self.scalar = Entry()
        self.scalar_exact = Entry()

        self.pressure = Entry()
        self.pressure_exact = Entry()

        self.xvelocity = Entry()
        self.xvelocity_exact = Entry()
        self.yvelocity = Entry()
        self.yvelocity_exact = Entry()

class Vis:
    """Visualizer class"""
    def __init__(self, filename):
        """Creates visualizer"""
        self.file = open(sys.argv[1], 'r')
        self.string = self.file.read()
        self.frames = []
        self.nx = 0
        self.ny = 0

    def parse(self):
        """Parses input file"""
        i = 0
        state = "" #None/NX/NY/T/X/Y/H/EH/S/ES/P/EP/V/EV
        buffer = ""
        x = np.empty(0)
        y = np.empty(0)
        xcounter = 0
        ycounter = 0

        # States:
        # 1. state == "", buffer == "": reading name (initial state)
        # 2. state == "", buffer != "": reading name
        # 3. state != "", buffer == "": reading value or name
        # 4. state != "", buffer != "": reading value
        while True:
            # 1. Reading name
            if state == "" and buffer == "":
                if i == len(self.string) or self.string[i] in { " ", "\t", "\n", "\r" }: pass
                else: buffer += self.string[i].upper()
            # 2. Reading name
            elif state == "" and buffer != "":
                if i == len(self.string) or self.string[i] in { " ", "\t", "\n", "\r" }:
                    if not buffer in { "NX", "NY", "T", "X", "Y", "H", "EH", "S", "ES", "P", "EP", "VX", "EVX", "VY", "EVY" }: raise Exception("Invalid name")
                    if buffer == "X": x = np.empty(0)
                    if buffer == "Y": y = np.empty(0)
                    state = buffer
                    buffer = ""
                else:
                    buffer += self.string[i].upper()
            # 3. Reading value or name
            elif state != "" and buffer == "":
                if i == len(self.string) or self.string[i] in { " ", "\t", "\n", "\r" }:
                    pass
                elif self.string[i].isalpha():
                    state = ""
                    buffer += self.string[i].upper()
                else:
                    buffer += self.string[i].upper()
            # 4. Reading value
            else:
                if i == len(self.string) or self.string[i] in { " ", "\t", "\n", "\r" }:
                    if state == "T": self.frames.append(Frame(float(buffer)))
                    elif state == "NX": self.nx = int(buffer)
                    elif state == "NY": self.ny = int(buffer)
                    elif state == "X": x = np.append(x, float(buffer))
                    elif state == "Y": y = np.append(y, float(buffer))
                    else:
                        if state == "H":    self.frames[-1].potential.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "EH": self.frames[-1].potential_exact.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "S":  self.frames[-1].scalar.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "ES": self.frames[-1].scalar_exact.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "P":  self.frames[-1].pressure.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "EP": self.frames[-1].pressure_exact.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "VX": self.frames[-1].xvelocity.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "EVX":self.frames[-1].xvelocity_exact.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "VY": self.frames[-1].yvelocity.add_point(x[xcounter], y[ycounter], float(buffer))
                        elif state == "EVY":self.frames[-1].yvelocity_exact.add_point(x[xcounter], y[ycounter], float(buffer))
                        else: raise Exception("Invalid state")
                        xcounter += 1
                        if xcounter == len(x):
                            xcounter = 0
                            ycounter += 1
                            if ycounter == len(y): ycounter = 0
                    buffer = ""
                else:
                    buffer += self.string[i].upper()
            if i == len(self.string): break
            i += 1

    def interpolate(self):
        """Find boundaries and generate matrices"""
        for frame in self.frames:
            frame.boundary = Boundary()
            if frame.potential.valid(): frame.boundary.merge(Boundary(frame.potential.array))
            if frame.potential_exact.valid(): frame.boundary.merge(Boundary(frame.potential_exact.array))
            if frame.scalar.valid(): frame.boundary.merge(Boundary(frame.scalar.array))
            if frame.scalar_exact.valid(): frame.boundary.merge(Boundary(frame.scalar_exact.array))
            if frame.pressure.valid(): frame.boundary.merge(Boundary(frame.pressure.array))
            if frame.pressure_exact.valid(): frame.boundary.merge(Boundary(frame.pressure_exact.array))
            if frame.xvelocity.valid(): frame.boundary.merge(Boundary(frame.xvelocity.array))
            if frame.xvelocity_exact.valid(): frame.boundary.merge(Boundary(frame.xvelocity_exact.array))
            if frame.yvelocity.valid(): frame.boundary.merge(Boundary(frame.yvelocity.array))
            if frame.yvelocity_exact.valid(): frame.boundary.merge(Boundary(frame.yvelocity_exact.array))

            if frame.potential.valid(): frame.potential.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.potential_exact.valid(): frame.potential_exact.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.scalar.valid(): frame.scalar.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.scalar_exact.valid(): frame.scalar_exact.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.pressure.valid(): frame.pressure.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.pressure_exact.valid(): frame.pressure_exact.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.xvelocity.valid(): frame.xvelocity.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.xvelocity_exact.valid(): frame.xvelocity_exact.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.yvelocity.valid(): frame.yvelocity.generate_matrix(frame.boundary, self.nx, self.ny)
            if frame.yvelocity_exact.valid(): frame.yvelocity_exact.generate_matrix(frame.boundary, self.nx, self.ny)                

    def visualize_matrices(self, names, times, boundaries, matrices):
        """"Visualizes structured matrices"""
        if len(times) == 1:
            # Horizontal
            figure, axes = plt.subplots(1, len(names), figsize=(5*len(names), 5))
            if len(names) == 1: axes = [axes]
            for column in range(len(names)):
                axes[column].set_title(names[column])
                b = boundaries[0]
                image = axes[column].imshow(matrices[column][0].T, extent=[b.xmin, b.xmax, b.ymin, b.ymax], origin='lower')
                figure.colorbar(image, ax=axes[column])
        else:
            # Vertical
            figure, axes = plt.subplots(len(names), len(times), figsize=(5*len(times), 5*len(names)))
            if len(names) == 1: axes = [axes]
            for row in range(len(names)):
                zmin = np.Infinity
                zmax = -np.Infinity
                for column in range(len(times)):
                    zmin = np.min((zmin, np.nanmin(matrices[row][column])))
                    zmax = np.max((zmax, np.nanmax(matrices[row][column])))
                for column in range(len(times)):
                    axes[row][column].set_title(names[row] + " (T = " + str(times[column]) + ")")
                    b = boundaries[column]
                    image = axes[row][column].imshow(matrices[row][column].T, extent=[b.xmin, b.xmax, b.ymin, b.ymax], vmin=zmin, vmax=zmax, origin='lower')
                figure.colorbar(image, ax=axes[row,:])

    def visualize(self, filename):
        """Visualizes input"""
        
        if len(self.frames) == 0: raise Exception("Nothing to visualize")
        if all([frame.potential.valid() and frame.potential_exact.empty()
            and frame.scalar.empty() and frame.scalar_exact.empty()
            and frame.pressure.empty() and frame.pressure_exact.empty()
            and frame.xvelocity.empty() and frame.xvelocity_exact.empty()
            and frame.yvelocity.empty() and frame.yvelocity_exact.empty()
            for frame in self.frames ]):
            # Scenario 1: potential plot without exact value
            self.visualize_matrices(
                ["Potential"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[frame.potential.matrix for frame in self.frames]])
            
        elif all([frame.potential.valid() and frame.potential_exact.valid()
            and frame.scalar.empty() and frame.scalar_exact.empty()
            and frame.pressure.empty() and frame.pressure_exact.empty()
            and frame.xvelocity.empty() and frame.xvelocity_exact.empty()
            and frame.yvelocity.empty() and frame.yvelocity_exact.empty()
            for frame in self.frames ]):
            # Scenario 2: potential plot with exact value
            self.visualize_matrices(
                ["Potential", "Potential error"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[frame.potential.matrix for frame in self.frames], [frame.potential_exact.matrix - frame.potential.matrix for frame in self.frames]])
            
        elif all([frame.potential.empty() and frame.potential_exact.empty()
            and frame.scalar.valid() and frame.scalar_exact.empty()
            and frame.pressure.empty() and frame.pressure_exact.empty()
            and frame.xvelocity.empty() and frame.xvelocity_exact.empty()
            and frame.yvelocity.empty() and frame.yvelocity_exact.empty()
            for frame in self.frames ]):
            # Scenario 3: scalar plot without exact value
            self.visualize_matrices(
                ["Scalar"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[frame.scalar.matrix for frame in self.frames]])
            
        elif all([frame.potential.empty() and frame.potential_exact.empty()
            and frame.scalar.valid() and frame.scalar_exact.valid()
            and frame.pressure.empty() and frame.pressure_exact.empty()
            and frame.xvelocity.empty() and frame.xvelocity_exact.empty()
            and frame.yvelocity.empty() and frame.yvelocity_exact.empty()
            for frame in self.frames ]):
            # Scenario 4: scalar plot with exact value
            self.visualize_matrices(
                ["Scalar", "Scalar error"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[frame.scalar.matrix for frame in self.frames], [frame.scalar_exact.matrix - frame.scalar.matrix for frame in self.frames]])
            
        elif all([frame.potential.empty() and frame.potential_exact.empty()
            and frame.scalar.empty() and frame.scalar_exact.empty()
            and frame.pressure.empty() and frame.pressure_exact.valid()
            and frame.xvelocity.valid() and frame.xvelocity_exact.valid()
            and frame.yvelocity.valid() and frame.yvelocity_exact.valid()
            for frame in self.frames ]):
            # Scenario 5: velocity plot with exact value
            self.visualize_matrices(
                ["Velocity", "Velocity error"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[np.sqrt(np.square(frame.xvelocity.matrix) + np.square(frame.yvelocity.matrix)) for frame in self.frames],
                 [np.sqrt(np.square(frame.xvelocity.matrix - frame.xvelocity_exact.matrix) + np.square(frame.yvelocity.matrix - frame.yvelocity_exact.matrix)) for frame in self.frames]])
            
        elif all([frame.potential.empty() and frame.potential_exact.empty()
            and frame.scalar.empty() and frame.scalar_exact.empty()
            and frame.pressure.valid() and frame.pressure_exact.valid()
            and frame.xvelocity.valid() and frame.xvelocity_exact.valid()
            and frame.yvelocity.valid() and frame.yvelocity_exact.valid()
            for frame in self.frames ]):
            # Scenario 6: velocity and pressure plot with exact value
            self.visualize_matrices(
                ["Velocity", "Velocity error", "Pressure", "Pressure error"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[np.sqrt(np.square(frame.xvelocity.matrix) + np.square(frame.yvelocity.matrix)) for frame in self.frames],
                 [np.sqrt(np.square(frame.xvelocity.matrix - frame.xvelocity_exact.matrix) + np.square(frame.yvelocity.matrix - frame.yvelocity_exact.matrix)) for frame in self.frames],
                 [frame.pressure.matrix for frame in self.frames],
                 [frame.pressure_exact.matrix - frame.pressure.matrix for frame in self.frames]])

        elif all([frame.potential.empty() and frame.potential_exact.empty()
            and frame.scalar.empty() and frame.scalar_exact.empty()
            and frame.pressure.valid() and frame.pressure_exact.empty()
            and frame.xvelocity.valid() and frame.xvelocity_exact.empty()
            and frame.yvelocity.valid() and frame.yvelocity_exact.empty()
            for frame in self.frames ]):
            # Scenario 7: velocity and pressure plot without exact value
            self.visualize_matrices(
                ["Velocity", "Velocity error"],
                [frame.time for frame in self.frames],
                [frame.boundary for frame in self.frames],
                [[np.sqrt(np.square(frame.xvelocity.matrix) + np.square(frame.yvelocity.matrix)) for frame in self.frames],
                 [frame.pressure.matrix for frame in self.frames]])
            
        else:
            raise Exception("Invalid scenario")
        
        if filename is None: plt.show()
        else: plt.savefig(filename, bbox_inches='tight', pad_inches=0)

if __name__ == "__main__":
    if len(sys.argv) != 2 and len(sys.argv) != 3: raise Exception("Invalid usage")
    vis = Vis(sys.argv[1])
    vis.parse()
    vis.interpolate()
    vis.visualize(sys.argv[2] if len(sys.argv) == 3 else None)