# Numsimulation
Numsimulation is project developed as part of Numerical Flow Simulation course at RWTH Aachen university. It is a collection of seven software libraries with every one responsible for its own type of problem.

### Dependencies
 - The library depends on [Eigen](https://eigen.tuxfamily.org) for its core functionality.
 - [CMake](https://cmake.org) is used for compilation and further automation
 - [Python](https://www.python.org), [NumPy](https://numpy.org) and [Matplotlib](https://matplotlib.org) are used for visualization. 
 - [Doxygen](https://www.doxygen.nl) is used for generation of documentation
 - [LaTeX](https://www.latex-project.org) and [Latexmk](https://mg.readthedocs.io/latexmk.html) are used for report generation

### Build
```
mkdir build
cd build
cmake ..
cmake --build .
```

### Usage
```
cd build
cmake --build . --target demo   # Run all tests and visualize results
cmake --build . --target doc    # Generate documentation
cmake --build . --target report # Generate report
```