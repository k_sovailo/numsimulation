/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential_rectangular/potential_rectangular.h>
#include <numsimulation/common.h>
using namespace numsimulation;
using namespace potential_rectangular;
#include <stdexcept>
#include <iostream>
#include <math.h>

int _main(int argc, char **argv)
{
    //Parsing arguments
    if (argc != 3) throw std::runtime_error("Invalid argument number");

    //Creating parameters object
    ParameterReader reader(argv[1]);
    AbstractParameters *parameters;
    struct ParallelParameters : AbstractParameters
    {
        double vinf[2];
        virtual double potential(Eigen::Vector2d coord) const { return vinf[0] * coord.x() + vinf[1] * coord.y(); }
        ParallelParameters(ParameterReader &reader) : AbstractParameters(reader),
            vinf{reader.get_real("VINFX"), reader.get_real("VINFY")}
            { exact = true; }
    };
    struct StagnationPointParameters : AbstractParameters
    {
        double a;
        virtual double potential(Eigen::Vector2d coord) const { return a * (sqr(coord.x()) - sqr(coord.y())); }
        StagnationPointParameters(ParameterReader &reader) : AbstractParameters(reader),
            a(reader.get_real("A")) 
            { exact = true; }
    };
    if (reader.get_string("SCENARIO") == "PARALLEL") parameters = new ParallelParameters(reader);
    else if (reader.get_string("SCENARIO") == "STAGNATION_POINT") parameters = new StagnationPointParameters(reader);
    else throw std::runtime_error("_main(): Unrecognized scenario");
    
    //Unused parameter warning
    reader.warn_unused();

    //Setting simulation up
    Solver solver(parameters, argv[2]);
    solver.solve();
    solver.output();
    return 0;
}

int main(int argc, char **argv)
{
    try
    {
        return _main(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << "\n";
        return 1;
    }
}