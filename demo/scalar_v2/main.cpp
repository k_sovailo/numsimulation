/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <scalar_v2/scalar_v2.h>
#include <numsimulation/common.h>
using namespace numsimulation;
using namespace scalar_v2;
#include <stdexcept>
#include <iostream>
#include <math.h>

/// Wave function that is a square function -F/2 (-L/2<x<0), F/2 (0<x<L/2) on time t, but begins to diffuse over time
double wave(double L, double F, double density, double diffusion, double x, double t, unsigned int N)
{
    const double failsafe = 0.99;
    if (x < -failsafe*(L / 2)) x = -failsafe*(L / 2);
    else if (x > failsafe*(L / 2)) x = failsafe*(L / 2);
    double result = 0;
    for (unsigned int n = 0; n < N; n++)
    {
        unsigned int k = 1 + 2 * n;
        result += (F / 2) * (4 / M_PI) * (sin(k * M_PI * x / L) / k) * exp(-t * diffusion * sqr(M_PI) * sqr(k) / sqr(L) / density);
    }
    return result;
}

int _main(int argc, char **argv)
{
    //Parsing arguments
    if (argc != 3) throw std::runtime_error("Invalid argument number");

    //Creating parameters object
    ParameterReader reader(argv[1]);
    AbstractParameters *parameters;
    struct ParallelParameters : AbstractParameters
    {
        double min[2];
        double max[2];
        Eigen::Vector2d v;
        double start;
        double density;
        double diffusion;
        double s;
        unsigned int precision;

        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const { return v; }
        virtual double scalar(Eigen::Vector2d coord, double time) const
        {
            double y_center = (min[1] + max[1])/2 + coord.x() / v(0) * v(1);
            return s/2 + wave(max[1] - min[1], s, density, diffusion, coord.y() - y_center, (coord.x() - start) / v(0), precision);
        }
        ParallelParameters(ParameterReader &reader) : AbstractParameters(reader),
            min{reader.get_real("XMIN"), reader.get_real("YMIN")},
            max{reader.get_real("XMAX"), reader.get_real("YMAX")},
            v(reader.get_real("VX"), reader.get_real("VY")),
            start(reader.get_real("START")),
            density(reader.get_real("DENSITY")),
            diffusion(reader.get_real("DIFFUSION")),
            s(reader.get_real("SCALAR")),
            precision(reader.get_integer("PRECISION"))
        {
            boundaries.resize(4);
            boundaries[0].figure = new Line(Eigen::Vector2d(max[0], max[1]), Eigen::Vector2d(max[0], min[1]), false);  //Right
            boundaries[1].figure = new Line(Eigen::Vector2d(max[0], min[1]), Eigen::Vector2d(min[0], min[1]), false);  //Bottom
            boundaries[2].figure = new Line(Eigen::Vector2d(min[0], min[1]), Eigen::Vector2d(min[0], max[1]), false);  //Left
            boundaries[3].figure = new Line(Eigen::Vector2d(min[0], max[1]), Eigen::Vector2d(max[0], max[1]), false);  //Top
            boundaries[0].fixed_scalar = true; //Right
            boundaries[1].fixed_scalar = false;//Bottom
            boundaries[2].fixed_scalar = true; //Left
            boundaries[3].fixed_scalar = false;//Top
            grid_origin = Eigen::Vector2d((min[0] + max[0]) / 2, (min[1] + max[1]) / 2);
            exact = true;
        }
    };
    struct PerpendicularParameters : AbstractParameters
    {
        double min[2];
        double max[2];
        Eigen::Vector2d v;
        double density;
        double diffusion;
        double s;
        unsigned int precision;

        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const { return v; }
        virtual double scalar(Eigen::Vector2d coord, double time) const
        {
            return s/2 + wave(max[0] - min[0], s, density, diffusion, -((min[0]+max[0])/2 - coord.x()) - v(0)*time, time, precision);
        }
        PerpendicularParameters(ParameterReader &reader) : AbstractParameters(reader),
            min{reader.get_real("XMIN"), reader.get_real("YMIN")},
            max{reader.get_real("XMAX"), reader.get_real("YMAX")},
            v(reader.get_real("VX"), reader.get_real("VY")),
            density(reader.get_real("DENSITY")),
            diffusion(reader.get_real("DIFFUSION")),
            s(reader.get_real("SCALAR")),
            precision(reader.get_integer("PRECISION"))
        {
            boundaries.resize(4);
            boundaries[0].figure = new Line(Eigen::Vector2d(max[0], max[1]), Eigen::Vector2d(max[0], min[1]), false);  //Right
            boundaries[1].figure = new Line(Eigen::Vector2d(max[0], min[1]), Eigen::Vector2d(min[0], min[1]), false);  //Bottom
            boundaries[2].figure = new Line(Eigen::Vector2d(min[0], min[1]), Eigen::Vector2d(min[0], max[1]), false);  //Left
            boundaries[3].figure = new Line(Eigen::Vector2d(min[0], max[1]), Eigen::Vector2d(max[0], max[1]), false);  //Top
            boundaries[0].fixed_scalar = true; //Right
            boundaries[1].fixed_scalar = false;//Bottom
            boundaries[2].fixed_scalar = true; //Left
            boundaries[3].fixed_scalar = false;//Top
            grid_origin = Eigen::Vector2d((min[0] + max[0]) / 2, (min[1] + max[1]) / 2);
            exact = true;
        }
    };
    if (reader.get_string("SCENARIO") == "PARALLEL") parameters = new ParallelParameters(reader);
    else if (reader.get_string("SCENARIO") == "PERPENDICULAR") parameters = new PerpendicularParameters(reader);
    else throw std::runtime_error("_main(): Unrecognized scenario");
    
    //Read the rest of parameters
    unsigned int steps = reader.get_integer("STEPS");
    unsigned int divider = reader.get_integer("DIVIDER");
    reader.warn_unused();

    //Simulation
    Solver solver(parameters, argv[2]);
    solver.output();
    for (unsigned int step = 0, divider_step = 0; step < steps; step++)
    {
        solver.step();
        divider_step++;
        if (divider_step == divider)
        {
            solver.output();
            divider_step = 0;
        }
    }
    return 0;
}

int main(int argc, char **argv)
{
    try
    {
        return _main(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << "\n";
        return 1;
    }
}