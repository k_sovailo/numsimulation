/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <potential/potential.h>
#include <numsimulation/common.h>
using namespace numsimulation;
using namespace potential;
#include <stdexcept>
#include <iostream>
#include <math.h>

int _main(int argc, char **argv)
{
    //Parsing arguments
    if (argc != 3) throw std::runtime_error("Invalid argument number");

    //Creating parameters object
    ParameterReader reader(argv[1]);
    AbstractParameters *parameters;
    struct ParallelParameters : AbstractParameters
    {
        double vinfx;
        virtual double potential(Eigen::Vector2d coord) const { return vinfx * coord.x(); }
        ParallelParameters(ParameterReader &reader) : AbstractParameters(reader),
        vinfx(reader.get_real("VINFX"))
        {
            boundaries.resize(4);
            boundaries[0].figure = new Line(Eigen::Vector2d(reader.get_real("XMAX"), reader.get_real("YMAX")), Eigen::Vector2d(reader.get_real("XMAX"), reader.get_real("YMIN")), false);
            boundaries[1].figure = new Line(Eigen::Vector2d(reader.get_real("XMAX"), reader.get_real("YMIN")), Eigen::Vector2d(reader.get_real("XMIN"), reader.get_real("YMIN")), false);
            boundaries[2].figure = new Line(Eigen::Vector2d(reader.get_real("XMIN"), reader.get_real("YMIN")), Eigen::Vector2d(reader.get_real("XMIN"), reader.get_real("YMAX")), false);
            boundaries[3].figure = new Line(Eigen::Vector2d(reader.get_real("XMIN"), reader.get_real("YMAX")), Eigen::Vector2d(reader.get_real("XMAX"), reader.get_real("YMAX")), false);
            boundaries[0].typ = BoundaryType::fixed;
            boundaries[1].typ = BoundaryType::wall;
            boundaries[2].typ = BoundaryType::fixed;
            boundaries[3].typ = BoundaryType::wall;
            grid_origin = Eigen::Vector2d((reader.get_real("XMIN") + reader.get_real("XMAX")) / 2, (reader.get_real("YMIN") + reader.get_real("YMAX")) / 2);
            exact = true;
        }
    };
    struct RadialParameters : AbstractParameters
    {
        double center_potential;
        double radius;
        virtual double potential(Eigen::Vector2d coord) const { return (coord.norm() < radius) ? center_potential : 0.0; }
        RadialParameters(ParameterReader &reader) : AbstractParameters(reader),
        center_potential(reader.get_real("POTENTIAL")),
        radius((reader.get_real("INNER") + reader.get_real("OUTER")) / 2)
        {
            boundaries.resize(2);
            boundaries[0].figure = new Circle(Eigen::Vector2d::Zero(), reader.get_real("INNER"), false);
            boundaries[1].figure = new Circle(Eigen::Vector2d::Zero(), reader.get_real("OUTER"), true);
            boundaries[0].typ = BoundaryType::fixed;
            boundaries[1].typ = BoundaryType::fixed;
            grid_origin = Eigen::Vector2d((reader.get_real("INNER") + reader.get_real("OUTER")) / 2, 0);
            exact = false;
        }
    };
    if (reader.get_string("SCENARIO") == "PARALLEL") parameters = new ParallelParameters(reader);
    else if (reader.get_string("SCENARIO") == "RADIAL") parameters = new RadialParameters(reader);
    else throw std::runtime_error("_main(): Unrecognized scenario");
    
    //Unused parameter warning
    reader.warn_unused();

    //Setting simulation up
    Solver solver(parameters, argv[2]);
    solver.solve();
    solver.output();
    return 0;
}

int main(int argc, char **argv)
{
    try
    {
        return _main(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << "\n";
        return 1;
    }
}