/* This file is a part of Numsimulation project
Developed as part of Numerical Flow Simulation course
Author: Kyrylo Sovailo */

#include <simple/simple.h>
#include <numsimulation/common.h>
using namespace numsimulation;
using namespace simple;
#include <stdexcept>
#include <iostream>
#include <math.h>

int _main(int argc, char **argv)
{
    //Parsing arguments
    if (argc != 3) throw std::runtime_error("Invalid argument number");

    //Creating parameters object
    ParameterReader reader(argv[1]);
    AbstractParameters *parameters;
    struct PoiseuilleParameters : AbstractParameters
    {
        double min[2], max[2];
        double p[2];
        double u[2];
        double density, viscosity;
        double a, b, c;
        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const
        {
            return Eigen::Vector2d(a + b * coord.y() + c * sqr(coord.y()), 0);
        }
        virtual double pressure(Eigen::Vector2d coord, double time) const
        {
            return p[0] + (p[1] - p[0]) * (max[0] - coord.x()) / (max[0] - min[0]);
        }
        PoiseuilleParameters(ParameterReader &reader) : AbstractParameters(reader),
            min{reader.get_real("XMIN"), reader.get_real("YMIN")},
            max{reader.get_real("XMAX"), reader.get_real("YMAX")},
            p{reader.get_real("PMIN"), reader.get_real("PMAX")},
            u{reader.get_real("VMIN"), reader.get_real("VMAX")},
            density(reader.get_real("DENSITY")),
            viscosity(reader.get_real("VISCOSITY"))
        {
            c = (p[1] - p[0]) / (2 * viscosity * (max[1] - min[0]));
            b = ((u[1] - u[0]) - c * (sqr(max[1]) - sqr(min[1]))) / (max[1] - min[1]);
            a = u[0] - b * min[1] - c * sqr(min[1]);
            boundaries.resize(4);
            boundaries[0].figure = new Line(Eigen::Vector2d(max[0], max[1]), Eigen::Vector2d(max[0], min[1]), false);  //Right
            boundaries[1].figure = new Line(Eigen::Vector2d(max[0], min[1]), Eigen::Vector2d(min[0], min[1]), false);  //Bottom
            boundaries[2].figure = new Line(Eigen::Vector2d(min[0], min[1]), Eigen::Vector2d(min[0], max[1]), false);  //Left
            boundaries[3].figure = new Line(Eigen::Vector2d(min[0], max[1]), Eigen::Vector2d(max[0], max[1]), false);  //Top
            boundaries[0].fixed_pressure = true;
            boundaries[1].fixed_pressure = false;
            boundaries[2].fixed_pressure = false;
            boundaries[3].fixed_pressure = false;
            grid_origin = Eigen::Vector2d((min[0] + max[0]) / 2, (min[1] + max[1]) / 2);
            exact = true;
        }
    };
    struct RotationParameters : AbstractParameters
    {
        double w;
        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const
        {
            return Eigen::Vector2d(-coord.y() * w, coord.x() * w);
        }
        virtual double pressure(Eigen::Vector2d coord, double time) const
        {
            return coord.squaredNorm() * sqr(w) * density / 2;
        }
        RotationParameters(ParameterReader &reader) : AbstractParameters(reader),
        w(reader.get_real("W"))
        {
            boundaries.resize(1);
            boundaries[0].figure = new Circle(Eigen::Vector2d::Zero(), reader.get_real("R"), true);
            boundaries[1].figure = new Line(Eigen::Vector2d(0,-0.5), Eigen::Vector2d(0,0.5), true);
            boundaries[0].fixed_pressure = false;
            boundaries[1].fixed_pressure = true;
            grid_origin = Eigen::Vector2d::Zero();
            exact = true;
        }
    };
    struct PipeParameters : AbstractParameters
    {
        virtual Eigen::Vector2d velocity(Eigen::Vector2d coord, double time) const
        {
            return Eigen::Vector2d(0.1, 0);
        }
        virtual double pressure(Eigen::Vector2d coord, double time) const
        {
            return 0;
        }
        PipeParameters(ParameterReader &reader) : AbstractParameters(reader)
        {
            boundaries.resize(10);
            boundaries[0].figure = new Line(Eigen::Vector2d(0, 1), Eigen::Vector2d(1.4, 1), false);
            boundaries[1].figure = new Line(Eigen::Vector2d(1.4, 1), Eigen::Vector2d(1.4, 0.4), false);
            boundaries[2].figure = new Line(Eigen::Vector2d(1.4, 0.4), Eigen::Vector2d(2, 0.4), false);
            boundaries[3].figure = new Line(Eigen::Vector2d(2, 0.4), Eigen::Vector2d(2, 0), false);
            boundaries[4].figure = new Line(Eigen::Vector2d(2, 0), Eigen::Vector2d(0.2, 0), false);
            boundaries[5].figure = new Line(Eigen::Vector2d(0.2, 0), Eigen::Vector2d(0.2, 0.6), false);
            boundaries[6].figure = new Line(Eigen::Vector2d(0.2, 0.6), Eigen::Vector2d(0.6, 0.6), false);
            boundaries[7].figure = new Line(Eigen::Vector2d(0.6, 0.6), Eigen::Vector2d(0.6, 0.8), false);
            boundaries[8].figure = new Line(Eigen::Vector2d(0.6, 0.8), Eigen::Vector2d(0, 0.8), false);
            boundaries[9].figure = new Line(Eigen::Vector2d(0, 0.8), Eigen::Vector2d(0, 1), false);
            boundaries[0].fixed_pressure = false;
            boundaries[1].fixed_pressure = false;
            boundaries[2].fixed_pressure = false;
            boundaries[3].fixed_pressure = true;
            boundaries[4].fixed_pressure = false;
            boundaries[5].fixed_pressure = false;
            boundaries[6].fixed_pressure = false;
            boundaries[7].fixed_pressure = false;
            boundaries[8].fixed_pressure = false;
            boundaries[9].fixed_pressure = false;
            grid_origin = Eigen::Vector2d(0.4, 0.4);
            exact = false;
        }
    };
    if (reader.get_string("SCENARIO") == "POISEUILLE") parameters = new PoiseuilleParameters(reader);
    else if (reader.get_string("SCENARIO") == "ROTATION") parameters = new RotationParameters(reader);
    else if (reader.get_string("SCENARIO") == "PIPE") parameters = new PipeParameters(reader);
    else throw std::runtime_error("_main(): Unrecognized scenario");
    
    //Read the rest of parameters
    unsigned int steps = reader.get_integer("STEPS");
    unsigned int divider = reader.get_integer("DIVIDER");
    reader.warn_unused();

    //Simulation
    Solver solver(parameters, argv[2]);
    solver.output();
    for (unsigned int step = 0, divider_step = 0; step < steps; step++)
    {
        solver.step();
        divider_step++;
        if (divider_step == divider)
        {
            solver.output();
            divider_step = 0;
        }
    }
    return 0;
}

int main(int argc, char **argv)
{
    try
    {
        return _main(argc, argv);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << "\n";
        return 1;
    }
}
