\chapter{scalar}
\label{scalar}
Die bisher berechnete Simulationen bezogen sich nur auf stationäre Strömungen. In diesem Kapitel wird es eine instationäre Strömung für ersten Mal betrachtet. Die zu beobachtende Größe auf dem Gebiet heißt nun ganz abstrakt \say{Skalar}. Jede intensive Größe kann in echten Leben als Skalar dienen, z.b. Temperatur oder Konzentration eines Stoffes. Dabei wird es angenommen, dass die Geschwindigkeit der Strömung am jeder Punkt und Zeitpunkt bekannt ist und wird durch eine Funktion $\vec{v}(x, y, t)$ beschrieben.

Zur Lösung der instationären Strömungen wird es die Methode der finiten Volumen verwendet. Dafür soll die Fläche nicht in Punkten, sondern in Zellen aufgeteilt werden. Eine Modifikation der Gittergenerationsalgorithmus wird benötigt. Der Algorithmus soll nicht mehr die Center der Zellen als Punkten betrachten, sondern die eigentlichen Punkten am Ränder der Zellen. Jetzt werden nicht Zellen selbst als \say{aktiv} oder \say{passiv} markiert, sondern jede einzelnen Punkte der Zellen. Das gewünschte Resultat solcher Algorithmus ist auf der Abbildung \ref{fig:scalar-1}  zu sehen. Die weißen Punkte bezeichnen dabei die ideale Position der Punkten, die schwarze bezeichnen die echte, durch die Untersuchung aller möglicher Überquerungen gefundene Punkte. Die Seite, die auf seinen perfekten Positionen sich befinden, werden als \say{regulare} Seiten bezeichnen (auch wenn die Seite nicht vollständig ist). Die Reste der Seiten sind die sogenannte \say{irregulare} Seiten. Der rote Punkt ist einen Massencenter der Zelle.

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.5\textwidth]{image/cut_polygon.drawio.pdf}
    \caption{Verbesserter Algorithmus}
    \label{fig:scalar-1}
\end{figure}

Die Abbildung \ref{fig:scalar-2} zeigt, wie die drei Arten von Gittern des modifizierten Algorithmus aussehen.

\begin{figure}[H]
    \begin{subfigure}[b]{0.35\textwidth}
        \centering
        \captionsetup{justification=centering}
        \includegraphics[width=1\textwidth]{image/complex_triangle.drawio.pdf}
        \caption{Dreiechiges Gitter}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \captionsetup{justification=centering}
        \includegraphics[width=1\textwidth]{image/complex_square.drawio.pdf}
        \caption{Quadratisches Gitter}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \captionsetup{justification=centering}
        \includegraphics[width=1\textwidth]{image/complex_hexagon.drawio.pdf}
        \caption{Hexagonales Gitter}
    \end{subfigure}

    \centering
    \captionsetup{justification=centering}
    \caption{Drei Arten von Gitter}
    \label{fig:scalar-2}
\end{figure}

Die Grenzen können entweder \say{fixiert} oder \say{nicht fixiert} sein. Die Zellen, die durch einer \say{fixierten} Grenze überquert werden, heißen auch \say{fixiert}. Der Skalarwert jeder \say{fixierter} Zelle wird als $\vec{v}(x, y, t)$ angenommen. Der Skalarwert jeder \say{nicht fixierter} Zelle wird mit einem normalen Vorgehen berechnet.

\newpage
Zuerst sollen es einige Definitionen eingeführt werden. Für eine Zelle (bezeichnet als Zelle $0$) und ihre Nachbar-Zelle (Zelle $i$):
\begin{itemize}
    \item Fluiddichte $\rho$, Diffusionskoeffziient $\lambda$, Faleche der Zelle $A$
    \item Skalarwerte $s_0$ und $s_i$
    \item Koordinate von Punkten $p_i = \begin{bmatrix}x_i \\ y_i\end{bmatrix}$ und $p_{i+1} = \begin{bmatrix}x_{i+1} \\ y_{i+1}\end{bmatrix}$
    \item Center der Seite $f_i = (p_i + p_{i+1}) / 2$
    \item Koordinate von Centern der Zellen $c_0$ und $c_i$
    \item Seietenvektor $\vec{L} = p_{i+1} - p{i}$
    \item Normale (nach innen) $\vec{\mathbf{n}} = \begin{bmatrix}0 & 1 \\ -1 & 0\end{bmatrix} \vec{L} / \sqrt{\vec{L}^2}$
    \item Korrigierte Seietenlaenge $L = \sqrt{\vec{L}^2 - (\vec{L} \cdot \vec{\mathbf{n}})^2}$
    \item Korrigierte Länge zwischen Zellencenter $l = (c_0 - c_i) \cdot \vec{\mathbf{n}}$
    \item Korrigierte Länge zwischen Zenter der Zelle und der Seite  $l_f = (c_0 - f_i) \cdot \vec{\mathbf{n}}$
    \item Fluidgeschwindigkeit an der Seite: $v_i = \vec{v}(f_i, t) \cdot \vec{\mathbf{n}}$
\end{itemize}


\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.5\textwidth]{image/precalculated.drawio.pdf}
    \caption{Verbesserter Algorithmus}
    \label{fig:scalar-3}
\end{figure}
Der Skalarwert ist nur auf Centern der Zellen definiert und der erste Schritt in den mathematischen Kern des Algorithmus ist es, den Wert des Skalars auf der Ränder zu finden. Wir betrachten sobald drei Methoden: lineare (\ref{eq:scalar-1}), exponentielle (\ref{eq:scalar-2}) und die Upwind-Methode (\ref{eq:scalar-3}). Den Skalarwert auf den Centern der Zelle wird als $s_{i,f}$ bezeichnet.

\begin{equation}
    \label{eq:scalar-1}
    s_{i,f} = \dfrac{s_0 (l_i - l_{i,f}) + s_i l_{i,f}}{l_i}
\end{equation}

\begin{equation}
    \label{eq:scalar-2}
    \begin{split}
        s_{i,f} = s_0 + (s_i - s_0) \dfrac{e^{Pe_i l_i / l_{i,f}} - 1}{e^{Pe_i} - 1} \\
        Pe_i = \dfrac{\rho v_i l_i}{\lambda}
    \end{split}
\end{equation}

\begin{equation}
    \label{eq:scalar-3}
    \begin{split}
        s_{i,f} = s_i, (v_i \geq 0) \\
        s_{i,f} = s_0, (v_i < 0)
    \end{split}
\end{equation}

Dann werden die konvektive und diffusive Terme des Flusses mit der Gleichung \ref{eq:scalar-4} gerechnet ($n$ steht für den Zeitschritt, $m$ steht für Anzahl der Nachbarn):
\begin{equation}
    \label{eq:scalar-4}
    \begin{split}
        f_{\mathrm{convective}, i} = \rho v_i s_{i,f} L_i \\
        f_{\mathrm{diffusive}, i} = \lambda (s_i - s_0) L_i / l_i \\
        s_0^{n+1} = s_0^n + \dfrac{\Delta t}{\rho A} \sum_{i = 1}^{m} f_{\mathrm{convective}, i} + f_{\mathrm{diffusive}, i}
    \end{split}
\end{equation}

\newpage
\section*{Beispliel 1}
In ersten Beispiel fließt das Fluid von links nach rechts. An Anfang hat die untere Hälfte $s = s_{\mathrm{min}}$, und die Obere $s = s_{\mathrm{max}}$, mit der Zeit und Stecke aber differieren die beiden Hälfte ineinander. Die linke und rechte Grenzen des Gebiets sind \say{fixiert}. Die exakte Lösung umfasst eine \say{Wellenfunktion}, sie ist in der Gleichung \ref{eq:scalar-ex1-1} definiert.
\begin{equation}
    \label{eq:scalar-ex1-1}
    \mathrm{wave}(L, F, \rho, \lambda, x, t) = \sum_{n = 0}^{n = 1000} \dfrac{F}{2} \dfrac{4}{\pi} \frac{\sin({k \pi x / L})}{k} e^{-t \dfrac{\lambda \pi^2 k^2}{L^2 \rho}}, k = 2 n + 1
\end{equation}

\begin{equation}
    \label{eq:scalar-ex1-2}
    \begin{split}
        \vec{v}(x, y, t) = \begin{bmatrix}0.1 & 0\end{bmatrix}^T \\
        s_{\mathrm{min}} = 0, s_{\mathrm{max}} = 1, \Delta t = 0.01, t_{\mathrm{max}} = 1, \rho = 1000, h_x = h_y = 0.02 \\
        x_{\mathrm{min}} = y_{\mathrm{min}} = -0.5, x_{\mathrm{max}} = y_{\mathrm{max}} = 0.5, x_{\mathrm{begin}} = -0.55 \\
        s_e(x, y, t) = \dfrac{s_{\mathrm{max}}}{2} + \mathrm{wave}(y_{\mathrm{max}} - y_{\mathrm{min}}, s_{\mathrm{max}}, \rho, \lambda, y - \dfrac{y_{\mathrm{min}} + y_{\mathrm{max}}}{2}, \dfrac{x - x_{\mathrm{begin}}}{v_x}) \\
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.75\textwidth]{../demo/scalar/1.png}
    \caption{Beispeil 1}
    \label{fig:scalar-ex1-1}
\end{figure}

\newpage
\section*{Beispliel 2}
Jetzt wird es ein Beispiel betrachtet, wo ein Gebiet von Fluid mit $s = s_{\mathrm{min}}$ befindet sich stationär neben einem Gebiet mit $s = s_{\mathrm{max}}$.

\begin{equation}
    \label{eq:scalar-ex2-1}
    \begin{split}
        \vec{v}(x, y, t) = \begin{bmatrix}0 \\ 0\end{bmatrix} \\
        s_{\mathrm{min}} = 0, s_{\mathrm{max}} = 1, \Delta t = 0.01, t_{\mathrm{max}} = 1, \rho = 1000, h_x = h_y = 0.02 \\
        x_{\mathrm{min}} = y_{\mathrm{min}} = -0.5, x_{\mathrm{max}} = y_{\mathrm{max}} = 0.5 \\
        s_e(x, y, t) = \dfrac{s_{\mathrm{max}}}{2} + \mathrm{wave}(x_{\mathrm{max}} - x_{\mathrm{min}}, s_{\mathrm{max}}, \rho, \lambda, -\dfrac{x_{\mathrm{max}} - x_{\mathrm{min}}}{2} + x, t)
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.8\textwidth]{../demo/scalar/2.png}
    \caption{Beispeil 2}
    \label{fig:scalar-ex2-1}
\end{figure}

\newpage
\section*{Beispliel 3}
Ähnlich zu dem vorherigen Beispiel, aber nun wird das Fluid auf dem ganzen Gebiet langsam links fließen

\begin{equation}
    \label{eq:scalar-ex3-1}
    \begin{split}
        \vec{v}(x, y, t) = \begin{bmatrix}0.1 \\ 0\end{bmatrix} \\
        s_{\mathrm{min}} = 0, s_{\mathrm{max}} = 1, \Delta t = 0.01, t_{\mathrm{max}} = 1, \rho = 1000, h_x = h_y = 0.02 \\
        x_{\mathrm{min}} = -0.5, x_{\mathrm{max}} = 0.5, y_{\mathrm{min}} = -0.5, y_{\mathrm{max}} = 0.5 \\
        s_e(x, y, t) = \dfrac{s_{\mathrm{max}}}{2} + \mathrm{wave}(x_{\mathrm{max}} - x_{\mathrm{min}}, s_{\mathrm{max}}, \rho, \lambda, -\dfrac{x_{\mathrm{max}} - x_{\mathrm{min}}}{2} + x - v_x t, t)
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.8\textwidth]{../demo/scalar/3.png}
    \caption{Beispeil 3}
    \label{fig:scalar-ex3-1}
\end{figure}
