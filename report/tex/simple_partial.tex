\chapter{simple\_partial}
\label{simple-partial}
In diesem Kapitel wird der sogenannte partielle SIMPLE-Algorithmus besprochen. Der Algorithmus ist ein wichtiger Zwischenschritt zu der Aufbau von einem vollständigen SIMPLE-Algorithmus. Er ist imstande, die zweidimensionale Geschwindigkeit der Strömung $\vec{v}(x, y, t)$ zu berechnen. Der Name \say{Partial} bezieht sich auf dem Fakt, dass der Druck wird nicht gerechnet, sondern von Nutzer explizit gegeben als die Funktion $p_e(x, y, t)$ ist.

Die Gleichung \ref{eq:simple-partial-1} ist eine Vektorformulierung von Impulserhaltungsgleichung. Die Gleichung \ref{eq:simple-partial-2} ist eine etwa entwickelte und umgeformte Formulierung von derselben Gleichung.
\begin{equation}
    \label{eq:simple-partial-1}
    \begin{split}
        \dfrac{\partial(\rho \vec{v})}{\partial t} + (\mathrm{div} \vec{\vec{F}}) = - \mathrm{grad}(p) \\
        \vec{v} = \begin{bmatrix}u \\ v\end{bmatrix} \\
        \vec{\vec{F}} = \begin{bmatrix}\rho u u - \eta \dfrac{\partial u}{\partial x} & \rho u v - \eta \dfrac{\partial v}{\partial x} \\ \rho v u - \eta \dfrac{\partial u}{\partial y} & \rho v v - \eta \dfrac{\partial v}{\partial x}\end{bmatrix}
    \end{split}
\end{equation}

\begin{equation}
    \label{eq:simple-partial-2}
    \begin{cases}
        \dfrac{\partial(\rho u)}{\partial t} = -\dfrac{\partial \Big((\rho u) u\Big)}{\partial x} +\dfrac{\partial}{\partial x}\Big(\eta \dfrac{\partial u}{\partial x}\Big) - \dfrac{\partial \Big((\rho v) u\Big)}{\partial y} +\dfrac{\partial}{\partial y}\Big(\eta \dfrac{\partial u}{\partial y}\Big) - \dfrac{\partial p}{\partial x}\\
        \dfrac{\partial(\rho v)}{\partial t} = -\dfrac{\partial \Big((\rho u) v\Big)}{\partial x} +\dfrac{\partial}{\partial x}\Big(\eta \dfrac{\partial v}{\partial x}\Big) - \dfrac{\partial \Big((\rho v) v\Big)}{\partial y} +\dfrac{\partial}{\partial y}\Big(\eta \dfrac{\partial v}{\partial y}\Big) - \dfrac{\partial p}{\partial y}
    \end{cases}
\end{equation}

Die Gleichung \ref{eq:simple-partial-3} ist die zweidimensionale Skalartransportgleichung aus dem Kapitel \ref{scalar} ($q$ ist dabei der Quellterm).

\begin{equation}
    \label{eq:simple-partial-3}
    \dfrac{\partial(\rho s)}{\partial t} = -\dfrac{\partial \Big((\rho u) s\Big)}{\partial x} +\dfrac{\partial}{\partial x}\Big(\eta \dfrac{\partial s}{\partial x}\Big) - \dfrac{\partial \Big((\rho v) s\Big)}{\partial y} +\dfrac{\partial}{\partial y}\Big(\eta \dfrac{\partial s}{\partial y}\Big) + q
\end{equation}

Vergleicht man die Gleichungen \ref{eq:simple-partial-2} und \ref{eq:simple-partial-3}, könnte man merken, dass diese fast identisch sind. Die nichtlineare Terme $\rho u u$ und $\rho v v$ werden dadurch linearisiert, dass man den ersten $u$ bzw. $v$ konstant annimmt. In der Gleichung \ref{eq:simple-partial-3} ist dann der Skalar $s$ mit Geschwindigkeit $u$ bzw. $v$ ersetzt, und der Quellterm $q$ - mit Druckgradient $-\dfrac{\partial p}{\partial x}$ bzw. $-\dfrac{\partial p}{\partial y}$. Das heißt, anstatt eine Skalartransportgleichung mit Skalar $s$ zu lösen, soll der Löser beide $u$-Gleichung und $v$-Gleichung lösen.

Die Geometrie des Gitters ist auch von Interesse hier. Anders als in vorherigen Kapitel, hier wird es nur quadratische Gitter verwendet, auf der Center von Zellen ist nur der Druck definiert, $u$ ist definiert an linken und rechten Seiten der Zellen, $v$ ist definiert an oberen und unteren Seiten. Eine Skizze einem solchen Gitter ist auf der Abbildung \ref{fig:simple-partial-1} zu sehen. $u$ und $v$ sind auf quadratischen Gebieten definiert, und sie formen seine eigenen Gitter, die bezüglich der Hauptgitter auf Halbschritt verschoben sind. Dieser Trick ist sehr einfach auf quadratischen Gitter zu machen. Auf dreieckigen und hexagonalen Gitter könnte man so auch tun, aber dann würden die Druckpunkte nicht auf der Seiten von $u$- und $v$-Quadraten liegen. Das Bauen von dreieckigen und hexagonalen Gitter ist nicht absolut unmöglich, aber man sollte dafür eine sehr smarte Geometrie erfinden. Dazu noch es ist viel einfacher von Programmiersicht, die quadratische Gitter zu implementieren. Deswegen habe ich entschieden, den Experiment mit verschiedenen Geometrien aufzuhören.

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.3\textwidth]{image/simple.drawio.pdf}
    \caption{Druckgitter, $u$-Gitter und $v$-Gitter}
    \label{fig:simple-partial-1}
\end{figure}

Die Hauptgleichung des Algorithmus (\ref{eq:simple-partial-4}) lautet also sehr ähnlich zu der Gleichung \ref{eq:scalar-v2-1}. Es werden aber nicht Nachbarn-Zellen rund um einer Zelle betrachtet, sondern die Nachbarn-Seiten rund um einer Seite. Um die geometrischen Maße mit Elementen aus Kapitel \ref{scalar} und \ref{simple} nicht zu verwechseln, wird ein Unterindex $f$ (für \say{Face}) benutzt. So ist $u_{f,0}$ die Geschwindigkeit in der vorläufigen Zelle und $u_i$ ist die Geschwindigkeit in Nachbarn-Zellen (diese Geschwindigkeiten dienen wie Skalar $s$ in Kapitel \ref{scalar}). $v_i$ bezeichnet die Geschwindigkeit, die in die Zelle ein- und ausfließt (und dient wie die Geschwindigkeit $v_i$ aus Kapitel \ref{scalar}). $\Delta P$ ist die Druckgradient zwischen den Punkten auf rechte und linke Seite des Quadrates. $L_f$ ist der Abstand zwischen Druckpunkte, $l_{f,i}$ ist der Anstand zwischen den Centern der Quadrate, $L_{f,i}$ ist die Länge der Grenze zwischen zwei Quadraten. Mit dem $\eta$ wird Zähigkeit bezeichnet. Eine Visualisierung von Verzeichnissen ist auf der Abbildung \ref{fig:simple-partial-3} zu sehen. Die Gleichung ist so generalisiert, dass beide $x$- und $y$-Geschwindigkeitskomponenten gerechnet werden können.

\begin{equation}
    \label{eq:simple-partial-4}
    \begin{split}
        \tilde{a}_0 u_i^{n+1} = b + \sum_{i = 1}^{m} a_i u_i^n + \dfrac{-\Delta P}{L_f} A_f\\
        b = a_0 u_{f,0} \\
        a_0 = \dfrac{\rho A_f}{\Delta t} \\
        \tilde{a}_0 = a_0 + \sum_{i = 1}^{m} a_i \\
        a_i = D_i L_{f,i} A(Pe_i) + \max(0, f_i L_{f,i}) \\
        D_i = \dfrac{\eta}{l_i}, Pe_i = \dfrac{\rho v_i l_{f,i}}{\eta}, f_i = v_i \rho
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.4\textwidth]{image/face_simple.drawio.pdf}
    \caption{Bezeichnungen auf Geschwindigkeit-Quadraten}
    \label{fig:simple-partial-3}
\end{figure}

\newpage
\section*{Beispliel 1}
Dieses Beispiel zeigt die Couette-Strömung, die selbst ein Unterfall von Poiseuille-Strömung ist. Die Poiseuille-Strömung findet in einer rechteckigen Kiste statt. Die Geschwindigkeit der unteren Platte ist $v_0$, die Geschwindigkeit der oberen Platte ist $v_1$. Der Druck auf rechte Seite ist als $P_\mathrm{min}$ definiert und auf der linken Seite als $P_\mathrm{max}$.

Die exakte stationäre Lösung der Poiseuille-Strömung ist auf der Gleichung \ref{eq:simple-partial-ex1-1} zu sehen. Die Werte der Konstanten sind in den Gleichungen \ref{eq:simple-partial-ex1-2} gegeben.
\begin{equation}
    \label{eq:simple-partial-ex1-1}
    \begin{split}
        v_e(x, y, t) = 0 \\
        u_e(x, y, t) = A + B y + C y^2 \\
        A = v_0 - B y_{\mathrm{min}} - C y_{\mathrm{min}}^2 \\
        B = \dfrac{(v_1 - v_0) - C (y_{\mathrm{max}}^2 - y_{\mathrm{min}}^2)}{y_{\mathrm{max}} - y_{\mathrm{min}}} \\
        C = \dfrac{P_{\mathrm{max}} - P_{\mathrm{min}}}{2 \eta (x_{\mathrm{max}} - x_{\mathrm{min}})} \\
        P_e(x, y, t) = P_{\mathrm{min}} + (P_{\mathrm{max}} - P_{\mathrm{min}}) \dfrac{x_{\mathrm{max}} - x}{x_{\mathrm{max}} - x_{\mathrm{min}}}
    \end{split}
\end{equation}
\begin{equation}
    \label{eq:simple-partial-ex1-2}
    \begin{split}
        P_{\mathrm{min}} = 0, P_{\mathrm{max}} = 0 \\
        u_0 = -0.1, u_1 = 0.1 \\
        x_{\mathrm{min}} = y_{\mathrm{min}} = -0.5, x_{\mathrm{max}} = y_{\mathrm{max}} = 0.5 \\
        \Delta t = 0.01, t_{\mathrm{max}} = 1 \\
        \rho = 1000, \eta = 1
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.7\textwidth]{../demo/simple_partial/1.png}
    \caption{Couette-Strömung}
    \label{fig:simple-partial-ex1-1}
\end{figure}

Die visualisierte Geschwindigkeit und deren Fehler wird dabei als Norm der $x$- und $y$-Komponenten der Geschwindigkeit bzw. der Differenz berechnet. Es lohnt es sich auch zu erwähnen, dass die Fehler auf der Grenzen sind in Mehrheit die Fehler der Visualisierung. Das ist eine Folge davon, dass die $x$-Komponente und $y$-Komponente der Geschwindigkeit nicht auf denselben Orten definiert sind. Da die exakte Lage der Zeller und Seiten nicht manuell programmiert wird, sondern durch Gittergenerator erstellt, ist solche Situationen fast unmöglich zu vermeiden, besonders auf gekrümmte Grenzen. Würde man getrennt $x$- und $y$-Komponenten betrachten, würden die Fehler kleiner anscheinen.

\newpage
\section*{Beispiel 2}
Dieses Beispeil zeigt die Poiseuille-Strömung

\begin{equation}
    \label{eq:simple-partial-ex2-1}
    \begin{split}
        P_{\mathrm{min}} = 0, P_{\mathrm{max}} = 1 \\
        u_0 = -0.1, u_1 = 0.1 \\
        x_{\mathrm{min}} = y_{\mathrm{min}} = -0.5, x_{\mathrm{max}} = y_{\mathrm{max}} = 0.5 \\
        \Delta t = 0.01, t_{\mathrm{max}} = 1 \\
        \rho = 1000, \eta = 1
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.7\textwidth]{../demo/simple_partial/2.png}
    \caption{Poiseuille-Strömung}
    \label{fig:simple-partial-ex2-1}
\end{figure}

\newpage
\section*{Beispiel 3}
Dieses Beispiel zeigt die Körperrotation. Es findet auf einem runden Gebiet statt, dabei rotiert sich der Rand des Gebietes mit einer Winkelgeschwindigkeit $\omega$. Die exakte stationäre Lösung der Strömung ist auf der Gleichung \ref{eq:simple-partial-ex3-1} zu sehen.
\begin{equation}
    \label{eq:simple-partial-ex3-1}
    \begin{split}
    u_e(x, y, t) = -y \omega \\
    v_e(x, y, t) = x \omega \\
    P_e(x, y, t) = \dfrac{(x^2+y^2) \omega^2 \rho}{2}
    \end{split}
\end{equation}
\begin{equation}
    \label{eq:simple-partial-ex3-2}
    \begin{split}
        \omega = 0.1 \\
        R = 1 \\
        \Delta t = 0.01, t_{\mathrm{max}} = 1 \\
        \rho = 1000, \eta = 1
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.7\textwidth]{../demo/simple_partial/3.png}
    \caption{Körperrotation}
    \label{fig:simple-partial-ex3-1}
\end{figure}
