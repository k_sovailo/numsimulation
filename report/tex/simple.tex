\chapter{simple}
\label{simple}

In diesem Kapitel wird der SIMPLE-Algorithmus implementiert. Der Algorithmus ist imstande, die Strömungen beliebiger Geometrie zu simulieren, bei den es keine explizite und richtige Druck- oder Geschwindigkeitsfeld gegeben wird. Es gibt eine Menge der Gleichungen, die besprochen werden sollen, bevor es um eigentlichen Algorithmus geht. In diesem Kapitel werden alle Schätzungen der Größen mit dem Index $*$ bezeichnet, die Korrekturen der Größen haben den Index $\prime$. Der eigentliche Wert werden ohne Index geschrieben.

Die \textbf{erste} Gleichung ist die Gleichung \ref{eq:simple-1}, sie ist sehr ähnlich zu der Gleichung \ref{eq:simple-4} mit der Unterschied, dass die Gleichung keine mehr Zeitschritten hat, sondern ist implizit formuliert und formt ein quasilineares Gleichungssystem.

\begin{equation}
    \label{eq:simple-1}
    \tilde{a}_0 u_0^* = b + \sum_{i = 1}^{m} a_i u_i^* + \dfrac{-\Delta P^* A_f}{L_f}
\end{equation}

Das Gleichungssystem \ref{eq:simple-1} kann auch mit einem iterativen Verfahren gelöst werden. Es gibt keine Notwendigkeit, diese umzuformen, wie es in Kapitel \ref{potential} gemacht wurde, das System ist schon zu der Iteration bereit. Die Gleichung, die den Iterationsschritt und das Residuum beschreibt, ist auf der Gleichung \ref{eq:simple-2} zu sehen. Die Ausgangsbedingungen sind wie früher: $r^\nu \leq r_{\mathrm{u,max}}$ ist oder $\nu > \nu_{\mathrm{u,max}}$.

\begin{equation}
    \label{eq:simple-2}
    \begin{split}
        u_0^{*\nu+1} = \dfrac{1}{\tilde{a}_0} (b + \sum_{i = 1}^{m} a_i u_i^{*\nu} + \dfrac{-\Delta P^* A_f}{L_f}) \\
        r^\nu = b + \sum_{i = 1}^{m} a_i u_i^{*\nu} + \dfrac{-\Delta P^* A_f}{L_f} - \tilde{a}_0 u_0^{*\nu}
    \end{split}
\end{equation}

Die \textbf{zweite} Gleichung ist die Gleichung \ref{eq:simple-3}, sie steht für Korrektion von Geschwindigkeitsfeld. Diese Gleichung stammt aus der Gleichung \ref{eq:simple-1}, $b$ wurde dabei weggelassen, da es um Korrekturen geht, $\sum_{i = 1}^{m} a_i u_i^\prime$ wurde vernachlässigt.

\begin{equation}
    \label{eq:simple-3}
    \begin{split}
        u_0 = u_0^* + u_0^\prime = u_0^* + \dfrac{-\Delta P^\prime A_f}{L_f \tilde{a}_0}
    \end{split}
\end{equation}

Die \textbf{dritte} Gleichung ist die Gleichung \ref{eq:simple-4}, sie ist für die Druckkorrektur verantwortlich.
\begin{equation}
    \label{eq:simple-4}
    \begin{split}
        P = P^* + P^\prime
    \end{split}
\end{equation}

Um die \textbf{vierte} Gleichung zu herleiten, muss man die Kontinuitätsgleichung \ref{eq:simple-5} betrachten.
\begin{equation}
    \label{eq:simple-5}
    \begin{split}
        (\rho^{\nu+1} - \rho^\nu) \dfrac{A}{\Delta t} = \sum_{i=1}^m \rho v_i L_i
    \end{split}
\end{equation}

Setzt man die Geschwindigkeiten $u_i$ aus der Gleichung \ref{eq:simple-3} ein und nimmt man die Inkompressibilität der Strömung an, so bekommt man die Gleichung für die Druckkorrektur \ref{eq:simple-6}. Da das eine Zelle-zentrierte Gleichung ist, sind die Bezeichnungen ähnlich zu den aus dem Kapitel \ref{scalar}. Die grafische Illustration ist auf der Abbildung \ref{fig:simple-1} zu sehen.
\begin{equation}
    \label{eq:simple-6}
    \begin{split}
        \sum_{i=1}^m L_i v_i^* + \sum_{i=1}^m \dfrac{P_i^\prime A_{f,i}}{L_{f,i} \tilde{a}_i} - P_0^\prime \sum_{i=1}^m \dfrac{A_{f,i}}{L_{f,i} \tilde{a}_i} = 0
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.5\textwidth]{image/cell_simple.drawio.pdf}
    \caption{Bezeichnungen auf Zellen}
    \label{fig:simple-1}
\end{figure}

Die Gleichung \ref{eq:simple-6} ist auch eine implizite Gleichung, die durch das Iterieren lösbar ist. Der Integrationsschritt und das Residuum ist auf der Gleichung \ref{eq:simple-7} gezeigt. Das Iterieren stoppt, wenn die Bedingung $r^\nu \leq r_{\mathrm{u,max}}$ ist oder $\nu > \nu_{\mathrm{u,max}}$ erreicht ist. Wenn aber $e$ klein genug ist, ist das ein Indikator, dass die Kontinuität auf dem ganzen Gebiet erreicht ist und es ist die Zeit, zu der nächsten Zeitschritt zu springen.
\begin{equation}
    \label{eq:simple-7}
    \begin{split}
        \tilde{c}_0 = \sum_{i=1}^m c_i, c_i = \dfrac{A_{f,i}}{L_{f,i} \tilde{a}_i}, e = \sum_{i=1}^m L_i v_i^* \\
        P^{\prime \nu+1} = \dfrac{1}{\tilde{c}_0} (e + \sum_{i=1}^m c_i P_i^{\prime \nu}) \\
        r^\nu = e + \sum_{i=1}^m c_i P_i^{\prime \nu} - \tilde{c}_0 P_0^{\prime \nu}
    \end{split}
\end{equation}

Der Algorithmus sieht dann folgendermaßen aus:
\begin{itemize}
    \item Erstelle das Gitter und initialisiere den Löser
    \item Für jeden Zeitschritt $t$:
    \begin{itemize}
        \item Für jede Iteration $N$:
        \begin{itemize}
            \item Vorbereite den Löser für die Gleichung \ref{eq:simple-1} nach $u^*$ gegeben $P^*$
            \item Für jede Iteration $\nu$:
            \begin{itemize}
                \item Wiederhole den Iterationsschritt \ref{eq:simple-2} bis das Residuum is klein genug oder die Anzahl der Iterationen ist zu groß
            \end{itemize}
            \item Vorbereite den Löser die Gleichung \ref{eq:simple-6} nach $P^\prime$ gegeben $P^*$
            \item Für jede Iteration $\nu$:
            \begin{itemize}
                \item Wiederhole den Iterationsschritt \ref{eq:simple-7} bis das Residuum is klein genug oder die Anzahl der Iterationen ist zu groß
            \end{itemize}
            \item Berechne neue Geschwindigkeit $u$ mit der Gleichung \ref{eq:simple-3} und zuweise $u* \leftarrow u$
            \item Berechne neuen Druck $P$ mit der Gleichung \ref{eq:simple-4} und zuweise $P^* \leftarrow P$
            \item Verlasse die Schleife, falls $e < e_{\mathrm{max}}$ oder $N > N_{\mathrm{max}}$ ist
        \end{itemize}
        \item Setze die Zeit vor: $t \leftarrow t + \Delta t$
        \item Verlasse die Schleife falls $t >= t_{\mathrm{end}}$
    \end{itemize}
\end{itemize}

\newpage
\section*{Beispliel 1}

Die Beispeile in diesem Kapitel sind dieselbe wie in Kapitel \ref{simple-partial}, mit der Unterschied, dass es wird nicht nut Geschwindigkeit, sondern auch Druck mit richtigen werten vergliechen.

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.7\textwidth]{../demo/simple/1.png}
    \caption{Couette-Strömung}
    \label{fig:simple-ex1-1}
\end{figure}

\newpage
\section*{Beispiel 2}
Dieses Beispeil zeigt die Poiseuille-Strömung

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.7\textwidth]{../demo/simple/2.png}
    \caption{Poiseuille-Strömung}
    \label{fig:simple-ex2-1}
\end{figure}

\newpage
\section*{Beispiel 3}
Das Beispeil mit der Koerperrotation wurde etwa veraendert. Weil der Rand des Gebietes soll beise fuer Fluid nicht durchlaessig sein (d.h. die Geschwindigkeit auf dem Rand ist Null) und auch den Gewissen Druck haben. Das ist aber wegen der limitationen der Softwarebibliotek unmoeglich. Deswegen wurde es entschieden, nut eine Haelfte des Gebietes zu betracten. Der Druck dabei ist auf der lineare Teil definiert.


\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.6\textwidth]{../demo/simple/3.png}
    \caption{Körperrotation}
    \label{fig:simple-ex3-1}
\end{figure}

\newpage
\section*{Beispiel 4}
In dem Anwendungsbeispiel ist ein Rohr mit undurchlässigen Wanden modelliert. Der linke obere Teil lasst das Fluid in System ein. Der rechte untere Teil hat einen \say{fixierten} Druck und sorgt als eine Senke. Das etwa mehr dunklen Gebiet in das linke untere Teil zeiht auf die Formung eines Wirbels.

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=1\textwidth]{../demo/simple/4.png}
    \caption{Körperrotation}
    \label{fig:simple-ex4-1}
\end{figure}
