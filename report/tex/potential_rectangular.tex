\chapter{potential\_rectangular}
\label{potential-rectangular}

Potenzialströmungen sind das einfachste Problem im Rahmen des Kurses. In der Realität können die langsame, reibungsfreie und wirbelfreie Strömungen (wie z.B. Strömungen durch poröse Körper wie Sand oder Schwamm) als Potenzialströmungen beschrieben werden. Jedem Punkt solcher Strömungen kann ein Potenzial $\phi$ zugeordnet werden. Dann wird die Strömung durch die Gleichungen \ref{eq:potential-rectangular-1} und \ref{eq:potential-rectangular-2} beschrieben. Dabei sind $x$ und $y$ die kartesische Koordinate und $\vec{v}(x, y)$ ist die Strömungsgeschwindigkeit.

\begin{equation}
    \label{eq:potential-rectangular-1}
    \Delta\phi(x, y) = \dfrac{\partial^2 \phi}{\partial x^2} + \dfrac{\partial^2 \phi}{\partial y^2} = 0
\end{equation}

\begin{equation}
    \label{eq:potential-rectangular-2}
    \vec{v}(x, y) = \nabla \phi
\end{equation}

Beim diskretisierten Problem ist die Funktion $\phi(x, y)$ nicht am gesamten Raum definiert, sondern nur auf einigen Punkten. Diese Punkte bilden ein Rechteck mit Seiten $x_{\mathrm{min}}$, $x_{\mathrm{max}}$, $y_{\mathrm{min}}$, $y_{\mathrm{max}}$. Die Koordinaten der Punkte können durch die Gleichungen \ref{eq:potential-rectangular-3} und \ref{eq:potential-rectangular-4} bestimmt werden. Dabei sind $i$ und $j$ die Indices der Punkte, $N_x$ und $N_y$ sind deren Anzahlen in entsprechenden Dimensionen.

\begin{equation}
    \label{eq:potential-rectangular-3}
    x_i = x_{\mathrm{min}} + (x_{\mathrm{max}} - x_{\mathrm{min}}) \dfrac{i}{N_x-1}, \forall i \in [0 .. N_x]
\end{equation}

\begin{equation}
    \label{eq:potential-rectangular-4}
    y_j = y_{\mathrm{min}} + (y_{\mathrm{max}} - y_{\mathrm{min}}) \dfrac{j}{N_y-1}, \forall j \in [0 .. N_y]
\end{equation}

Eine Darstellung solcher Diskretisierung ist auf der Abbildung \ref{fig:potential-rectangular-1} zu sehen. Dabei sind die roten Punkte die \say{freien} Punkte, und die blauen Punkte sind \say{fixiert}, auf den gilt die Dirichlet-Randbedingung. Der Wert von solchen Punkten wird am Anfang des Algorithmus mit einer vorgegebenen und expliziten Lösungsfunktion $\phi_e(x, y)$ initialisiert und im Lauf der Algorithmus nicht verändert. Die \say{freie} Punkte werden mit Nullen initialisiert.

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=0.5\textwidth]{image/potential_rectangular.drawio.pdf}
    \caption{Rechteckiges Gitter}
    \label{fig:potential-rectangular-1}
\end{figure}


Den Vektor aller freie Potenziale (auf Punkten $(i, j) \in (1..N_x-1, 1..N_y-1)$) wird als $\vec{\phi}$ bezeichnet. $\phi_{i,j}$ ist eine alternative Schreibweise für $\phi(x_i, y_i)$. $A$ bezeichnet den Differenzierungsoperator, der am Eingang ein Potenzial erhält und die Summe seiner zweiten partiellen Ableitungen liefert. Dann kann man die Gleichung \ref{eq:potential-rectangular-5} aufschreiben.

\begin{equation}
    \label{eq:potential-rectangular-5}
    A \vec{\phi} = \vec{0}
\end{equation}

Matrix $A$ ist dabei eine große dünnbesetzte Matrix, sie erscheint in Berechnungen nie in expliziter Form, und ihr expliziter Ausdruck wird daher nicht ableitet. Eine Vorstellung über die Elemente von Matrix $A$ kann man aber durch finite Differenzen bekommen.

\begin{equation}
    \begin{split}
        \label{eq:potential-rectangular-6}
        \Delta\phi(x_i, y_j) = \Big(\dfrac{\partial^2 \phi}{\partial x^2}\Big)_{x_i,y_j} + \Big(\dfrac{\partial^2 \phi}{\partial y^2}\Big)_{x_i,y_j} = \\
        \dfrac{\phi_{i-1,j} - 2 \phi_{i,j} + \phi_{i+1,j}}{\Delta x^2} + \dfrac{\phi_{i,j-1} - 2 \phi_{i,j} + \phi_{i,j+1}}{\Delta y^2} + O(\Delta x^3)+ O(\Delta y^3)
    \end{split}
\end{equation}

Die Gleichung \ref{eq:potential-rectangular-5} lässt sich über iterative Methoden, nämlich Jakobi-Methode (Gleichung \ref{eq:potential-rectangular-7}) und Gauß-Seidel-Methode (Gleichung \ref{eq:potential-rectangular-8}) lösen. Dabei ist $\nu$ ist die Iteration des Algorithmus, $N = (N_x-1)(N_y-1)$ und $\phi$ ist nicht durch zwei Indexen, sondern nur durch einen indexiert. Der faktische Unterschied zwischen den zwei Methoden besteht darin, dass beim Gauß-Seidel Algorithmus wird ein neu kalkulierten Wert von $\phi_k$ sofort wieder in Vektor $\vec{\phi}$ eingestellt, aber Jakobi-Algorithmus wartet mit der Einstellung bis nächste Iteration.

\begin{equation}
    \label{eq:potential-rectangular-7}
    \phi^{\nu+1}_k = \dfrac{1}{a_{kk}} \Bigg(b_k - \sum^{k-1}_{q=1} a_{kq} \phi^{\nu+1}_q - \sum^{N}_{q=k+1} a_{kq} \phi^{\nu}_q \Bigg)
\end{equation}

\begin{equation}
    \label{eq:potential-rectangular-8}
    \phi^{\nu+1}_k = \dfrac{1}{a_{kk}} \Bigg(b_k - \sum^{k-1}_{q=1} a_{kq} \phi^{\nu}_q - \sum^{N}_{q=k+1} a_{kq} \phi^{\nu}_q \Bigg)
\end{equation}

Unter der Kenntnis, wie die Elemente von Matrix $A$ aussehen und dass der Vektor $b$ Null ist, kann man den mathematischen Kern für Lösung der Potenzialströmungen auf rechteckigen  Gitter mit den Gleichungen \ref{eq:potential-rectangular-9} formulieren. Dabei steht $a$ für die Koeffizienten der Matrix $A$ und $c$ steht für das Resultat der Multiplikation von Reihe der Matrix $A$ mal $\vec{\phi}$. $r$ ist das Residuum, d.h. das Resultat der Multiplikation $A \vec{\phi}$. In Abhängigkeit davon, ob die Gauß-Seidel-Methode oder Jacobi-Methode verwendet ist, wird $\phi^{\nu+1}_{i,j}$ sofort in $\phi$ eingestellt oder am Ende von Iteration $\nu$.

\begin{equation}
    \label{eq:potential-rectangular-9}
    \begin{split}
    c^\nu_{i,j,\mathrm{no\_center}} = \dfrac{\phi^\nu_{i-1,j} + \phi^\nu_{i+1,j}}{\Delta x^2} +  \dfrac{\phi^\nu_{i,j-1} + \phi^\nu_{i,j+1}}{\Delta y^2} \\
    a^\nu_{i,j,\mathrm{center}} = \dfrac{-2}{\Delta x^2} + \dfrac{-2}{\Delta y^2} \\
    c^\nu_{i,j,\mathrm{center}} = a^\nu_{i,j,\mathrm{center}} \phi^\nu_{i,j} \\
    \phi^{\nu+1}_{i,j} = -\dfrac{c^\nu_{\mathrm{i,j,no\_center}}}{a^\nu_{i,j,\mathrm{center}}} \\
    r^\nu_{i,j} = c^\nu_{i,j,\mathrm{no\_center}} + c^\nu_{i,j,\mathrm{center}}
    \end{split}
\end{equation}

Um das Konvergenz des Algorithmus zu beschleunigen oder um Stabilität zu erhöhen, es lässt sich ein Konzept von Overshooting/Undershooting zu implementieren. Die Gleichung \ref{eq:potential-rectangular-9} wird deswegen mit der Gleichung \ref{eq:potential-rectangular-10} ersätzt, wobei $\beta \in (0..2)$ ein Parameter ist.

\begin{equation}
    \label{eq:potential-rectangular-10}
    \phi^{\nu+1}_{i,j} = \beta ( -\dfrac{c^\nu_{\mathrm{i,j,no\_center}}}{a^\nu_{i,j,\mathrm{center}}}) + (1 - \beta) \phi^\nu_{i,j}
\end{equation}

Je kleiner ist $r$, desto naher ist des Algorithmus zur Lösung der Gleichung. Deswegen kann man für den Algorithmus zwei Ausgangsbedingungen formulieren:
\begin{itemize}
    \item $r \le r_{\mathrm{max}}$ für alle Punkte, $r_{\mathrm{max}}$ ist ein Parameter
    \item $\nu > \nu_{\mathrm{max}}$, $\nu_{\mathrm{max}}$ ist ein Parameter
\end{itemize}

\newpage
\section*{Beispiel 1}

Auf der Abbildung \ref{fig:potential-rectangular-ex1-1} ist eine Visualisierung der Arbeit des Algorithmus zu sehen. Der linke Teil zeigt die Extrapolation $\phi(x, y)$ von diskretem Resultat $\vec{\phi}$ auf dem kontinuierlichen Feld. Der rechte Teil zeigt die Differenz zwischen der bekommenen Lösung und der bekannten Lösung $\phi(x,y) - \phi_e(x,y)$:

\begin{equation}
    \label{eq:potential-rectangular-ex1-1}
    \begin{split}
        N_x = N_y = 101 \\
        x_{\mathrm{max}} = 0, x_{\mathrm{max}} = 1 \\
        y_{\mathrm{min}} = 0, y_{\mathrm{max}} = 1 \\
        v_{x, \inf} = 1 \\
        v_{y, \inf} = 0 \\
        \phi_e(x, y) = x v_{x, \inf} + y v_{y, \inf}
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=1\textwidth]{../demo/potential_rectangular/1.png}
    \caption{Parallelströmung}
    \label{fig:potential-rectangular-ex1-1}
\end{figure}

\section*{Beispiel 2}

\begin{equation}
    \label{eq:potential-rectangular-ex2-1}
    \begin{split}
        N_x = N_y = 101 \\
        x_{\mathrm{min}} = -1, x_{\mathrm{max}} = 1 \\
        y_{\mathrm{min}} = -1, y_{\mathrm{max}} = 1 \\
        A = 1 \\
        \phi_e(x, y) = A (x^2 - y^2)
    \end{split}
\end{equation}

\begin{figure}[H]
    \centering
    \captionsetup{justification=centering}
    \includegraphics[width=1\textwidth]{../demo/potential_rectangular/2.png}
    \caption{Staupunkt}
    \label{fig:potential-rectangular-ex2-1}
\end{figure}
